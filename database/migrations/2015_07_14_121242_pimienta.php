<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pimienta extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('client_id')->unsigned();
			$table->foreign('client_id')->references('id')->on('users');
			$table->integer('producer_id')->unsigned();
			$table->foreign('producer_id')->references('id')->on('users');
			$table->string('code')->unique();
			$table->string('name');
			$table->smallInteger('status')->default(1);
            $table->text('description')->nullable();
			$table->date('date_start');
			$table->date('date_end');
			$table->softDeletes();
			$table->timestamps();
		});

		Schema::create('indicators', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('project_id')->unsigned();
			$table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
			$table->string('name');
			$table->string('type');
			$table->string('details');
			$table->string('group');
			$table->timestamps();
		});

		/*
		 * Listado de activaciones para una campaña
		 */
		Schema::create('activations', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('project_id')->unsigned();
			$table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->string('week');
            $table->string('area');
            $table->tinyInteger('status');
            $table->string('code_sap');
			$table->string('code_local');
            $table->string('contact_pos');
			$table->string('type');
			$table->string('lat');
			$table->string('lon');
			$table->date('date');
			$table->time('h_start');
            $table->time('h_end');
			$table->string('sub_canal')->nullable();
            $table->string('sub_canalv2')->nullable();//“On” para identificar a aquellos locales en donde se realiza el consumo de producto dentro del mismo y “Off”  compra y consumir en otro lugar.
            $table->string('client')->nullable();
            $table->string('company')->nullable();
            $table->string('address')->nullable();
            $table->string('district')->nullable();
            $table->string('reference')->nullable();
            $table->string('supervisor')->nullable();
            $table->string('supervisor_phone')->nullable();
            $table->string('agent_comercial')->nullable();
            $table->string('agent_phone')->nullable();
            $table->text('observation')->nullable();
			$table->timestamps();
		});

		/*
		 * Guarda el listado de recursos disponibles para ser usados en la campaña
		 */
		Schema::create('resources', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('project_id')->unsigned();
			$table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->text('description')->nullable();
			$table->integer('quantity');
			$table->timestamps();
		});


		/*
		 * Guarda la relación entre la activación y los usuarios
		 * Se guarda el tipo de relación usando la propiedad type, puede ser supervisor, anfitriona, etc
		 */
		Schema::create('activation_user', function ($table) {
			$table->integer('activation_id')->unsigned();
			$table->foreign('activation_id')->references('id')->on('activations');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->string('type');
			$table->timestamps();
		});
        /*
             * Guarda la relación entre la Campaña y los usuarios
             * Se guarda el tipo de relación usando la propiedad type, puede ser supervisor, anfitriona, etc
             */
        Schema::create('project_user', function ($table) {
            $table->integer('project_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('projects');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
		/*
		 * Guarda la relación entre la activación y los indicadores y el valor asignado para ese indicador en la activación
		 * Tambien guarda la relación con el usuario que genero el reporte, normalmente es el supervisor encargado pero puede ser el productor activo
		 */
		Schema::create('reports', function ($table) {
            $table->increments('id');
			$table->integer('indicator_id')->unsigned();
			$table->foreign('indicator_id')->references('id')->on('indicators');

			$table->integer('activation_id')->unsigned();
			$table->foreign('activation_id')->references('id')->on('activations');

			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');

			$table->string('value');
			$table->string('observation');
            $table->date('date');
            $table->time('hour');
			$table->timestamps();
		});


        //Proceso de la activacion
        Schema::create('processes', function ($table) {
            $table->increments('id');
            $table->integer('activation_id')->unsigned();
            $table->foreign('activation_id')->references('id')->on('activations');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('type');
            $table->timestamps();
        });

        //Guarda las imagenes de los procesos, recursos materiales, y perfiles de los usuarios
        Schema::create('images', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('imageable_id')->unsigned();
            $table->string('imageable_type');
            //$table->foreign('process_id')->references('id')->on('processes');
            $table->string('name');
            $table->string('path');
            $table->text('description')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });


        Schema::create('teams', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->string('rol');
            $table->tinyInteger('number');
            $table->text('description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('team_activation', function ($table) {
            $table->integer('team_id')->unsigned();
            $table->foreign('team_id')->references('id')->on('teams');
            $table->integer('activation_id')->unsigned();
            $table->foreign('activation_id')->references('id')->on('activations');
            $table->timestamps();
        });

	}

	/*
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		Schema::drop('projects');
		Schema::drop('indicators');
		Schema::drop('reports');
		Schema::drop('activation_user');
		Schema::drop('resources');
		Schema::drop('activations');
        Schema::drop('teams');
	}

}
