<?php

use App\Project;
use App\Role;
use App\User;
use App\Image;
use App\Activation;
use App\Process;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use App\Commands\Status;

class DatabaseSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$rAdmin = Role::create(['name' => 'admin', 'display_name' => 'Administrador', 'description' => 'Administrador de sistema']);
		$rClient = Role::create(['name' => 'client', 'display_name' => 'Cliente', 'description' => 'CCliente']);
		Role::create(['name' => 'productor', 'display_name' => 'Productor', 'description' => 'Encargado de los proyectos']);
		Role::create(['name' => 'supervisor', 'display_name' => 'Supervisor', 'description' => 'Supervisor de las Activaciones']);
		Role::create(['name' => 'colaborador', 'display_name' => 'Colaborador', 'description' => 'Equipo de apoyo']);

		$admin = User::create([
			'name' => 'Gerson',
			'last_name' => 'Aduviri',
			'email' => 'gerson@geekadvice.pe',
			'password' => Hash::make('123456')
		]);

		$client = User::create([
			'name' => 'Backus',
			'last_name' => 'Apellido',
			'company' => 'Backus',
			'email' => 'backus@geekadvice.pe',
			'password' => Hash::make('123456')
		]);

        Image::create([
            'imageable_id'=>'2',
            'imageable_type'=>'App\User',
            'path' => 'img/avatar/001.png'

        ]);


        Image::create([
            'imageable_id'=>'1',
            'imageable_type'=>'App\User',
            'path' => 'img/avatar/001.png'
        ]);

		$admin->attachRole($rAdmin);
		$client->attachRole($rClient);

		$project_a=Project::create([
			'client_id' => $client->id,
			'producer_id' => $admin->id,
			'name' => 'Backus ICE',
			'code' => 'backus-ice',
			'description' => 'Descripción',
			'date_start' => Carbon::now(),
			'date_end' => Carbon::now()->addDay(5),
		]);

		$project_b=Project::create([
			'client_id' => $client->id,
			'producer_id' => $admin->id,
			'name' => 'Campaña de promociones',
			'code' => 'campana-promociones',
			'description' => 'Descripción',
			'date_start' => Carbon::now(),
			'date_end' => Carbon::now()->addDay(5),
		]);

        $activation_a=Activation::create([
            'project_id'=>$project_a->id,
            'code_sap'=>'25477785474',
            'code_local'=>'244774',
            'contact_pos'=>'Sr Roberto',
            'lat'=>'-11.95822',
            'lon'=>'77.08914',
            'date'=>'77.08914',
            'h_start'=>'13:00:00',
            'h_end'=>'20:00:00',
            'sub_canal'=>'Pe On Tr Bodega Bar',
            'company'=>'Comercial Dominguez',
            'address'=>'Paraiso Del Norte Mz A Lt 03',
            'district'=>'San Martin De Porres',
            'reference'=>'Av Canta Callao con Av Central-TOTUS',
            'supervisor'=>'Oscar Mariluz',
            'supervisor_phone'=>'989257252',
            'agent_comercial'=>'Muñante Masias, Alexander Josue',
            'agent_phone'=>'961842829',
            'sub_canalv2'=>'off',
            'area'=>'lima2',
            'observation'=>'El POS se encuentra en un lugar de poco tránsito. La poca afluencia se aprovechó para la degustación pero no se generaron ventas de las variedades Cusqueña.'
        ]);


        $activation_b=Activation::create([
            'project_id'=>$project_b->id,
            'code_sap'=>'25477785474',
            'code_local'=>'244774',
            'contact_pos'=>'Sr Flaviano',
            'lat'=>'-11.95822',
            'lon'=>'77.08914',
            'date'=>'77.08914',
            'h_start'=>'15:00:00',
            'h_end'=>'21:00:00',
            'sub_canal'=>'Pe On Tr Bodega Bar',
            'company'=>'Comercial Dominguez',
            'address'=>'Paraiso Del Norte Mz A Lt 03',
            'district'=>'San Martin De Porres',
            'reference'=>'Av Canta Callao con Av Central-TOTUS',
            'supervisor'=>'Oscar Mariluz',
            'supervisor_phone'=>'989257252',
            'agent_comercial'=>'Diaz Roman, Israel Francis',
            'agent_phone'=>'958885412',
            'sub_canalv2'=>'on',
            'area'=>'lima3',
            'observation'=>'Se recuperó el tiempo restante que no se activó en Minimarket el Sol. El POS no contaba con stock a pesar de haber solicitado la atención del pedido. Cliente frecuentemente genera ventas de la marca en variedades. '
        ]);

        Process::create([
            'activation_id'=>$activation_a->id,
            'name'=>'SELLO ICE',
            'description'=>'Al ingreso de la discoteca, se ubicó a parte de nuestro team en el ingreso para darle la bienvenida a
                            los asistentes y colocarles el sello representativo de la marca, el cual solo se podía apreciar con luz
                            ultra violeta.',
            'type'=>'0'
        ]);

        Process::create([
            'activation_id'=>$activation_a->id,
            'description'=>'El único Branding de la marca
                            encontrado dentro del local fueron los
                            equipos de frío. Asimismo, el contenido
                            de los mismos, correspondía
                            principalmente a otras marcas.',
            'type'=>'1'
        ]);

        Process::create([
            'activation_id'=>$activation_b->id,
            'name'=>'BLOQUES DE HIELO',
            'description'=>'Un bloque de hielo de 2.30mts de altura fue instalado en uno de los extremos del
                            escenario, en los cuales se ubicó a uno de nuestros “congelados”, mientras que otro
                            invitaba a los asistentes a que se tomen fotos dentro del mismo.',
            'type'=>'0'
        ]);

        Process::create([
            'activation_id'=>$activation_b->id,
            'description'=>'Se encontró un mapping que podría ser
                            aprovechado por la marca, ya que el
                            público que frecuenta este local se
                            encuentra básicamente entre 18 y 24 años
                            y coincide con el target de la marca.',
            'type'=>'1'
        ]);


		// $this->call('UserTableSeeder');
	}

}
