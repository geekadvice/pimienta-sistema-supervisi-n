@extends('app')

@section('content')
<div class="column small-centered large-4 medium-5">
	<p class="text-center">
		<img src="{{ asset('img/logo.png') }}" alt="" width="200">
	</p>
	<h4>Registro de usuarios</h4>
	@if (count($errors) > 0)
	<div class="alert-box">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif

	<form role="form" method="POST" action="{{ url('/auth/register') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="input-float">
			<label for="name">Nombres y Apellidos</label>
			<input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Ingreso tu nombre y apellidos">
		</div>

		<div class="input-float">
			<label for="email">E-Mail</label>
			<input type="email" class="form-control" name="email" value="{{ old('email') }}">
		</div>

		<div class="input-float">
			<label for="password">Contraseña</label>
			<input type="password" class="form-control" name="password">
		</div>

		<div class="input-float">
			<label for="password_confirmation">Confirma tu contraseña</label>
			<input type="password" class="form-control" name="password_confirmation">
		</div>
		<p class="text-center">
			<button type="submit" class="medium-6 small-centered">Registrarme</button>
		</p>
		<p class="text-center">
			<a href="{{URL::to('auth/login')}}">Iniciar sesión con mi cuenta</a>
		</p>
	</form>
</div>

@endsection
