@extends('app')

@section('content')
<div class="column small-centered large-4 medium-5">
	<p class="text-center">
		<img src="{{ asset('img/logo.png') }}" alt="" width="200">
	</p>
	<h3>Recuperar mi contraseña</h3>
	<p>
		Ingresa el correo con el que te registraste y te enviaremos instrucciones para reestablecer tu contraseña.
	</p>
					@if (session('status'))
						<div class="alert-box success">
							{{ session('status') }}
						</div>
					@endif

					@if (count($errors) > 0)
						<div class="alert-box">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="input-icon">
							<label for="email"><i class="icon-envelope"></i></label>
							<input type="email" name="email" placeholder="Ingresa tu correo electrónico" value="{{ old('email') }}">
						</div>
						<p class="text-center">
							<button type="submit" class="medium-6 btn btn-primary">Envíar correo</button>
						</p>
						<p class="text-center">
							<a href="{{ url('auth/login') }}">Volver a login</a>
						</p>

</div>
@endsection
