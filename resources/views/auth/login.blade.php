@extends('app')

@section('content')
<div class="table-row">
	<div class="table-cell">
		<div class="column small-centered large-4 medium-5 text-center">
			<img src="{{ asset('img/logo.png') }}" alt="" width="200">
			<!--<object type="image/svg+xml" data="{{ asset('svg_sprites/logo.svg') }}"> <img src="{{ asset('img/logo.png') }}" alt=""> </object>-->

			<h4 class="text-left">Iniciar sesión</h4>
			<form role="form" method="POST" action="{{ url('/auth/login') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				@if (count($errors) > 0)
				<div class="alert-box">
					<strong>Informacion!</strong>
					@foreach ($errors->all() as $error)
					<li style="color: #ffffff">{{ $error }}</li>
					@endforeach
				</div>
				@endif

				<div class="input-icon">
					<label for="email"><i class="icon-user"></i></label>
					<input type="email" name="email" id="email" placeholder="Ingresa tu correo electrónico" value="{{ old('email') }}">
				</div>

				<div class="input-icon">
					<label for="password"><i class="icon-lock"></i></label>
					<input type="password" id="password" name="password" placeholder="Ingresa tu contraseña">
				</div>
				<input id="member" type="checkbox" name="remember"><label for="member">Mantener mi sesión activa</label>
				<p>
					<button type="submit" class="medium-6 small-centered">Ingresar</button>
				</p>
				<p><a href="{{ url('/password/email') }}">No recuerdo mi contraseña</a></p>
			</form>


		</div>
	</div>
</div>
@endsection
