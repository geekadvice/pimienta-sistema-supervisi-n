<div ng-controller="TeamController">
	<div class="column medium-4">
		<div class="block-white">
			<div class="content">
				<h4>Campañas activas</h4>
				<ul class="list-items-content">
					<li ng-show="factory.projects.data.length == 0"><span class="text-info"><i class="icon-info"></i> No hay campañas disponibles</span>
					</li>
					<li ng-repeat="p in factory.projects.data">
						<div class="right toolbox"><a href="#projects/{{p.code}}" class="icon-pencil"></a></div>
						<a href="#team/{{p.code}}">{{p.name}}</a><br>
						<span class="text-info">Fecha de inicio: {{p.date_start}}</span>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="column medium-8">
		<div class="block-white content" ng-show="editProject != null">
			<a href="#projects" class="right"><i class="icon-close icon16"></i></a>

			<h3>{{editProject.data.name || 'Cargando...'}}</h3>

			<div zf-tabs="">
				<!-- Detalle de campaña -->
				<div zf-tab="" title="Asignación de activaciones">
					<h5>Supervisor A</h5>
					<ul>
						<li>Activación A <span class="text-info">- Ejecutada</span></li>
						<li>Activación B <span class="text-info">- Pendiente</span></li>
					</ul>
					<p class="text-center"><a class="button small">Agregar POS's para activacion</a></p>
					<hr>
					<h5>SupervisorBA</h5>
					<ul>
						<li>Activación C <span class="text-info">- Ejecutada</span></li>
						<li>Activación D <span class="text-info">- Pendiente</span></li>
					</ul>
					<p class="text-center"><a class="button small">Agregar POS's para activacion</a></p>
				</div>

				<!-- End tab detalle campaña-->

				<div zf-tab="" title="Indicadores">

				</div>
				<div zf-tab="" title="Activaciones">
					<h5>Cargar archivo con activaciones</h5>
					<label for="file">Archivo:</label>
					<input type="file" name="file">
					<p>
						<button><i class="icon-cloud-upload"></i> Cargar y procesar archivo</button>
					</p>

					<p class="text-info">Puedes descargar el archivo con el formato de ejemplo desde <a target="_blank" href="<?php echo url('docs/formato_activaciones.xls'); ?>">AQUÍ</a></p>

					<hr>
					<h5>Activaciones cargadas</h5>
					<table width="100%">
						<thead>
						<tr>

							<th>Nombre</th>
							<th>Ubicación</th>
							<th>Referencia</th>
							<th>Productor</th>
							<th>Cliente</th>
							<th>Estado</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td colspan="6">No tenemos POS's para mostrar</td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

