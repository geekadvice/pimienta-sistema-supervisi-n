<div ng-controller="ProjectsController" ng-app="app">
<div id="editStatus" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <h3 id="modalTitle" class="text-center">Estado</h3>
    <div>
        <a class="close-reveal-modal right"><i class="icon-close icon16"></i></a>
        <label>Estado</label>
        <select name="type" id="type" ng-model="statusActivation">
            <option value="0">Activo</option>
            <option value="1">Pendiente</option>
            <option value="2">Cerrado</option>
        </select>
    </div>
    <br>
    <div class="text-center">
        <a id="btn2"  class="button" ng-click="UpdatestatusActivation()"><i class="icon-check" ></i> Guardar</a>
    </div>
</div>
    <div id="editActivation" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">

        <h3 id="modalTitle" class="text-center">Activación</h3>
        <div>
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
            <label>Observaciones:</label>
            <textarea ng-model="editedActivation.observation">

            </textarea>
            <br>

            <form>
              <fieldset>
                <legend>Agregar de Imagenes</legend>

                <div class="row">
                    <div class="large-12 columns">
                        <div class="button" ngf-select ngf-change="upload($files)" ngf-multiple="true"><i class="icon-images" ></i> Subir Imagen</div>
                        {{log.progress}}
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="large-12 columns">
                       <ul class="list-items-content" ng-repeat="i in editedActivation.images" ng-show="editedActivation.images.length!='0'">
                            <li>
                                <span>{{i.name}}</span>
                                <div class="right toolbox" style="padding: 0px">
                                    <a data-reveal-id="modalImage" ng-click="previewImage(i.path)" href="" class="icon-eye">
                                    </a> <a class="icon-trash" ng-click="deleteImage(i.id)"></a>
                                </div>
                            </li>
                       </ul>

                        <div data-alert class="alert-box secondary" ng-show="editedActivation.images.length=='0'">
                            No se encontraron imagenes.
                        </div>
                    </div>
                </div>
                </fieldset>
            </form>
        </div>
        <br>
        <div class="text-center">
            <a id="btn2"  class="button" ng-click="saveActivation()"><i class="icon-check" ></i> Guardar</a>
        </div>
    </div>

    <div id="modalImage" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
      <h4 id="modalTitle" class="text-center">Vista Previa</h4>
        <ul class="related_post_gallery wp_rp">
            <div class="text-center">
                 <img src="<?php echo URL::to ('/') ?>/{{preview}}">
            </div>
        </ul>
        <br>
        <div class="text-center">
            <a data-reveal-id="editActivation" class="button"><< Regresar</a>
        </div>
    </div>

    <div id="modalTeam" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
        <a class="close-reveal-modal right"><i class="icon-close icon16"></i></a>
        <h4 id="modalTitle" class="text-center">Grupos</h4>
          <div class="row">
            <div class="large-5 columns">
                Nombres y Apellidos:
                <angucomplete-alt id="collaboratorid"
                                  placeholder="Buscar"
                                  search-fields="name" title-field="name"
                                  pause="400"
                                  selected-object="setFormCollaborator"
                                  remote-url="<?php echo URL::to('api/v1/users/find-collaborator?code={{projectCode}}&number={{formCollaborator.number}}&search='); ?>"
                                  text-searching="Buscando..."
                                  input-class="form-control form-control-small"
                                  remote-url-data-field="data"
                    />


            </div>
            <div class="large-3 columns">
                Cargo:
                <input type="text" ng-model="formCollaborator.rol">
            </div>

             <div class="large-2 columns">
                Nro Grupo:
                <select required ng-model="formCollaborator.number" class="form-control" >
                     <option ng-repeat="number in collaborator.number_group" value={{number}}>{{number}}</option>
                </select>
            </div>

            <div class="large-2 columns">
                <br>
                <button  ng-click="saveCollaborator()"><i class="icon-plus"></i> Agregar</button>
            </div>
        </div>

        <table width="100%">
            <tr>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>DNI</th>
                <th>Cargo</th>
                <th>Nro grupo</th>
                <th></th>
            </tr>

            <tr ng-repeat="c in collaborator.team">
                <th>{{c.user.name}}</th>
                <th>{{c.user.last_name}}</th>
                <th>{{c.user.dni}}</th>
                <th><input type="text" ng-model="c.rol"></th>
                <th>
                    <select id={{$index}} ng-model="collaborator.team[$index].number" class="form-control" >
                        <option ng-repeat="number in collaborator.number_group">{{number}}</option>
                    </select>
                </th>
                <th>
                    <a ng-click="deleteCollaborator(c.id)" class="small button alert"><i class="icon-trash"></i> Eliminar</a>
                    <a ng-click="editCollaborator(c.id,c.number,c.rol)" class="small button"><i class="icon-check"></i> Guardar</a>
                </th>
            </tr>
        </table>
        <br>


        <!--<div class="text-center">
            <a ng-click="saveCollaborator()" class="button"><i class="icon-check"></i> Guardar</a>
        </div>-->
    </div>

    <div class="column medium-4">
		<div class="block-white">
			<div class="content">
				<div class="right">
                    <input ng-model="search.$" placeholder="buscar"></label>
					<a ng-show="user.roles[0].display_name=='Administrador'" href="#projects/create" class="button small"><i class="icon-plus"></i> Crear
						campaña</a>
				</div>
				<h4>Campañas</h4>
				<ul class="list-items-content">
                    <input id="projectId" type="hidden" value="{{projectCode}}">
					<li ng-show="factory.projects.data.length == 0"><span class="text-info"><i class="icon-info"></i> No hay campañas disponibles</span>
					</li>
					<li ng-repeat="p in factory.projects.data | filter:search:strict">
						<div class="right toolbox"><a href="#projects/{{p.code}}" class="icon-pencil"></a> <a
                                ng-show="user.roles[0].display_name=='Administrador'" 	ng-click="deleteProject(p)" class="icon-trash"></a></div>
						<span>{{p.name}}</span><br>
						<span class="text-info">Fecha de inicio: {{p.date_start}}</span>
                        <span ng-show="p.status==0" class="label">Activo</span>
                        <span ng-show="p.status==1"  class="success warning label">Pendiente</span>
                        <span ng-show="p.status==2"  class="success alert secondary round radius label">Cerrado</span>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="column medium-8">

		<div class="block-white content" ng-show="editProject != null">
			<a href="#projects" class="right"><i class="icon-close icon16"></i></a>
			<h3>{{editProject.data.name || '...'}}</h3>
			<tabset>
				<!-- Detalle de campaña -->
				<tab ng-click="select(0)" active="tabs[0].active" heading="Detalle de campaña">
					<form name="formProject" ng-submit="saveProject()">
						<div class="inner-column clearfix">

							<div class="column medium-6">
								<div class="input-float">
									<label for="name">Nombre de campaña</label>
									<input type="text" name="name" id="name" ng-model="editProject.data.name" placeholder="Nueva campaña" required="">
								</div>
                                <div ng-show="formProject.$submitted || formProject.name.$touched">
                                    <span ng-show="formProject.name.$error.required">Nombre requerido.</span>
                                </div>
								<div class="input-float">
									<label for="data_start">Fecha de inicio</label>
                                    <input  id="data_start" type="text" name="date_start" mask="9999-19-39" mask-repeat='1' placeholder="2015-08-10" ng-model="editProject.data.date_start" mask-restrict='reject' mask-clean='true' mask-validate='true' mask-limit='true' required="">
									
								</div>
                                <div ng-show="formProject.$submitted || formProject.date_start.$touched">
                                    <span ng-show="formProject.date_start.$error.required">Fecha inicial requerida.</span>
                                </div>
								<div class="input-float">
									<label for="">Fecha fin</label>
									
                                    <input  id="date_end" type="text" name="date_end" mask="9999-19-39" mask-repeat='1' placeholder="2015-08-10" ng-model="editProject.data.date_end" mask-restrict='reject' mask-clean='true' mask-validate='true' mask-limit='true' required="">
								</div>
                                <div ng-show="formProject.$submitted || formProject.date_end.$touched">
                                    <span ng-show="formProject.date_end.$error.required">Fecha final requerida.</span>
                                </div>
							</div>

							<!-- segunda columna -->

							<div class="column medium-6">
								<div class="input-float">
									<label for="">Centro de Costos</label>
									<input type="text" name="code" ng-model="editProject.data.code" required="" placeholder="codigo-campana">
								</div>
                                <div ng-show="formProject.$submitted || formProject.code.$touched">
                                    <span ng-show="formProject.code.$error.required">Fecha final requerida.</span>
                                </div>
								<div class="input-float">
									<label for="client_id">Cliente</label>
                                    <!--<select name="client_id" id="client_id"
											ng-model="editProject.data.client_id">
										<option value="1">Client</option>
									</select>-->
                                    <angucomplete-alt id="client" name="client"
                                                      placeholder="Buscar"
                                                      search-fields="name" title-field="name,company"
                                                      pause="400"
                                                      selected-object="setFormclient"
                                                      remote-url="<?php echo URL::to('api/v1/users/finduser?rol=2&search='); ?>"
                                                      text-searching="Buscando..."
                                                      input-class="form-control form-control-small"
                                                      remote-url-data-field="data"
                                                      initial-value="editProject.data.client.name"
                                                      field-required="true"
                                        />
								</div>
                                <div ng-show="formProject.$submitted || formProject.client.$touched">
                                    <span ng-show="formProject.client.$error.required">Cliente requerido.</span>
                                </div>
								<div class="input-float">
									<label for="producer_id">Productor</label>

									<!--<select name="producer_id" id="producer_id"
											ng-model="editProject.data.producer_id">
										<option value="1">Productor</option>
									</select>-->
                                    <angucomplete-alt id="producer" name="producer"
                                                      placeholder="Buscar"
                                                      search-fields="name" title-field="name"
                                                      pause="400"
                                                      selected-object="setFormproducer"
                                                      remote-url="<?php echo URL::to('api/v1/users/finduser?rol=3&search='); ?>"
                                                      text-searching="Buscando..."
                                                      input-class="form-control form-control-small"
                                                      remote-url-data-field="data"
                                                      initial-value="editProject.data.producer.name"
                                                      field-required="true"
                                        />
								</div>
                                <div ng-show="formProject.$submitted || formProject.producer.$touched">
                                    <span ng-show="formProject.producer.$error.required">Productor requerido.</span>
                                </div>
							</div>
						</div>
						<div class="input-float">
							<label for="">Información de la campaña</label>
							<textarea name="description" id="description" rows="5"
									  ng-model="editProject.data.description" required=""></textarea>
						</div>
                        <div ng-show="formProject.$submitted || formProject.description.$touched">
                            <span ng-show="formProject.description.$error.required">descripción requerida.</span>
                        </div>
						<!-- opciones adicionales -->
						<div class="inner-column clearfix">
							<div class="column medium-6">
								<div class="input-float">
									<label for="status">Estado de la campaña</label>
									<select   name="status" id="status" ng-model="editProject.data.status" required="">
                                        <option value="0">Activo</option>
										<option value="1" selected >Pendiente</option>
										<option value="2">Cerrada</option>
									</select>
								</div>
                                <div ng-show="formProject.$submitted || formProject.status.$touched">
                                    <span ng-show="formProject.status.$error.required">Estado requerido.</span>
                                </div>
							</div>
							<div class="column medium-6">
							</div>
						</div>

						<p class="text-center">
							<button ng-show="user.roles[0].display_name=='Administrador'"  type="submit">Guardar</button>
						</p>
					</form>
				</tab>

				<!-- End tab detalle campaña-->
                <div ng-show="projectCode!='create'">
                <tab ng-click="select(1)" active="tabs[1].active" heading="Indicadores">
                    <div class="clearfix inner-column">
                        <div class="column medium-7">
                            <h5>Indicadores de campaña</h5>

                            <ul class="list-items" >
                                <li ng-repeat="i in editProject.data.indicators">
                                    <h3 ng-if="editProject.data.indicators[$index-1].group!=i.group" >{{i.group}}</h3>

                                    <div class="right toolbox">
                                        <a  ng-click="editIndicator(i)" class="icon-pencil"></a>
                                        <a ng-show="user.roles[0].display_name=='Administrador'"  ng-click="deleteIndicator(i)" class="icon-trash"></a>
                                    </div>

                                    <span>{{i.name}}</span>
                                    <span ng-show="i.type=='booleano'" class="text-info"> - Si & No</span>
                                    <span ng-show="i.type!='booleano'" class="text-info"> - {{i.type}}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="column medium-5">
                            <h5>Agregar indicadores</h5>

                            <form name="formIndicator" ng-submit="saveIndicator(editedIndicator,formIndicator)">
                                <input type="hidden" name="id" ng-model="editedIndicator.id">

                                <div class="input-float">
                                    <label for="name">Nombre del indicador</label>
                                    <input type="text" name="name" id="name" ng-model="editedIndicator.name" required="">

                                </div>
                                <div ng-show="formIndicator.$submitted || formIndicator.name.$touched">
                                    <span ng-show="formIndicator.name.$error.required">Nombre de indicador requerido.</span>

                                </div>
                                <div class="input-float">
                                    <label for="details">Detalles</label>
                                    <input type="text" name="details" ng-model="editedIndicator.details" required="">
                                </div>
                                <div ng-show="formIndicator.$submitted || formIndicator.details.$touched">
                                    <span ng-show="formIndicator.details.$error.required">Detalles requerido.</span>

                                </div>
                                <div class="input-float">
                                    <label for="group">Grupo</label>
                                    <input type="text" name="group" ng-model="editedIndicator.group" >
                                </div>
                                <div class="input-float">
                                    <label for="type">Tipo</label>
                                    <select name="type" id="type" ng-model="editedIndicator.type" required="">
                                        <option value="cuantitativo">Cuantitativo</option>
                                        <option value="cualitativo">Cualitativo</option>
                                        <option value="booleano">Si & No</option>
                                        <option value="libre">Libre</option>
                                        <option value="imagen">Reporte con imagenes</option>
                                    </select>
                                </div>
                                <div ng-show="formIndicator.$submitted || formIndicator.type.$touched">
                                    <span ng-show="formIndicator.type.$error.required">Tipo requerido.</span>
                                </div>
                                <p class="text-center">
                                    <button ng-show="user.roles[0].display_name=='Administrador'"  type="submit">Guardar</button>
                                </p>
                            </form>
                        </div>
                    </div>
                </tab>
                <tab ng-click="select(2)" active="tabs[2].active"  heading="Activaciones">
                    <h5 ng-show="user.roles[0].display_name=='Administrador'"  >Cargar archivo con activaciones</h5>
                    <label ng-show="user.roles[0].display_name=='Administrador'"  for="file">Archivo:</label>
                    <form >
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <div ng-show="user.roles[0].display_name=='Administrador'"  class="button" ngf-select ngf-change="uploadExcel($files)" ngf-multiple="multiple" ngf-accept="'.xls'" accept=".xls">Cargar excel</div>
                        <div ng-show="bar" class="col-sm-2"><progressbar   value="excel.value" type="warning" >{{excel.progress}}</progressbar></div>
                        <!--<input codeproject={{projectCode}} id="excel_file" type="file" name="file" accept=".xls" value="<?php echo csrf_token(); ?>">-->
                    </form>
                    <!--<p>
                        <button id="selectfile" name="selectfile" onclick="excel()"><i class="icon-cloud-upload"></i> Cargar y procesar archivo</button>
                    </p>-->

                    <p ng-show="user.roles[0].display_name=='Administrador'"  class="text-info">Puedes descargar el archivo con el formato de ejemplo desde <a target="_blank" download href="<?php echo url('activaciones_ejemplo.xls'); ?>">AQUÍ</a></p>

                    <hr ng-show="user.roles[0].display_name=='Administrador'" >
                    <h5>Activaciones cargadas </h5>
                    <table width="100%">
                        <thead>
                        <tr>
                            <th>Local</th>
                            <th>Estado</th>
                            <th>Ubicación</th>
                            <th>Referencia</th>
                            <th>Supervisor</th>
                            <th>Cliente</th>
                            <th>Editar</th>
                        </tr>

                        </thead>
                        <tbody>
                        <tr ng-show="editProject.data.stions.length=='0'">
                            <td colspan="6" >No tenemos POS's para mostrar</td>
                        </tr>

                        <tr ng-repeat="a in editProject.data.activations">
                            <td>{{a.sub_canal}}     </td>
                            <td>
                                <a ng-click="selectstatus(a.id,a.status,$index)" ng-show="a.status==0" data-reveal-id="editStatus" >Activo</a>
                                <a ng-click="selectstatus(a.id,a.status,$index)" ng-show="a.status==1" data-reveal-id="editStatus" >Pendiente</a>
                                <a ng-click="selectstatus(a.id,a.status,$index)" ng-show="a.status==2" data-reveal-id="editStatus" >Cerrado</a>
                            </td>
                            <td>{{a.address}}</td>
                            <td>{{a.reference}}</td>
                            <td>{{a.supervisor}}</td>
                            <td>{{a.client}}</td>
                            <td>  <div class="right toolbox"> <a id="$index" ng-click="selectActivation(a.id)" data-reveal-id="editActivation" class="icon-pencil"></a> <a ng-show="user.roles[0].display_name=='Administrador'"  ng-click="deleteActivation(a.id)" class="icon-trash"></a> </div></td>
                        </tr>
                        </tbody>
                    </table>
                </tab>
                <!-- Reportes -->


                <tab heading="Equipo" ng-click="select(3)" active="tabs[3].active">
                     <div class="large-12 columns no-left-margin">
                            <p >
                                <a href ng-click="tab = 1" ng-class="{true: 'tabselected', false: ''}[tab == 1]"><i class="ii-file" ></i> Supervisor</a> |
                                <a href ng-click="tab = 2" ng-class="{true: 'tabselected', false: ''}[tab == 2]"><i class="ii-display"></i> Colaboradores</a>
                            </p>
                            <div ng-show="tab === 1">
                                <div data-alert class="alert-box secondary" ng-show="usersproject.data.length=='0'">
                                    No tenemos supervisores asignados a este proyecto.
                                </div>
                                <table width="100%" ng-show="usersproject.data.length!='0'">
                                    <thead>
                                    <tr>
                                        <th>Nombres</th>
                                        <th>Apellidos</th>
                                        <th>DNI</th>
                                        <th>Carga</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="item in usersproject.data">
                                        <th>{{item.name}}</th>
                                        <th>{{item.last_name}}</th>
                                        <th>{{item.dni}}</th>
                                        <th>{{item.carga}}</th>
                                        <th>
                                            <a ng-click="deleteSupervisor(item.id)" class="small button alert"><i class="icon-trash"></i> Eliminar</a>
                                            <a href="<?php echo URL::to('/#/{{projectCode}}/supervisor/activation/{{item.id}}'); ?>" class="button small"><i class="icon-check"></i> Activaciones</a>
                                        </th>
                                    </tr>
                                    </tbody>
                                </table>
                                <br>
                                <div class="row no-left-margin">
                                    <div class="large-6 columns no-left-margin">
                                        <angucomplete-alt id="members"
                                                          placeholder="Buscar"
                                                          search-fields="name" title-field="name"
                                                          pause="400"
                                                          selected-object="setFormUser"
                                                          remote-url="<?php echo URL::to('api/v1/users/find?code={{projectCode}}&search='); ?>"
                                                          text-searching="Buscando..."
                                                          input-class="form-control form-control-small"
                                                          remote-url-data-field="data"
                                            />
                                    </div>
                                    <div class="large-6 columns">
                                        <button ng-click="saveUsercampania()"><i class="icon-check"></i> Agregar</button>
                                    </div>
                                </div>
                            </div>
                            <div ng-show="tab === 2">
                                <div data-alert class="alert-box secondary" ng-show="group.length=='0'">
                                    No tenemos grupos asignados a este proyecto.
                                </div>
                                <div ng-repeat="(key,g) in group">
                                    <table style="  border:none;  margin-bottom: 0px" width="100%">
                                        <tr>
                                            <th>Equipo {{key}}</th>
                                            <th ng-show="g.supervisor!=null">Supervisor: {{g.supervisor}}</th>
                                            <th ng-show="g.supervisor==null">Supervisor: No disponible</th>

                                        </tr>
                                    </table>
                                    <table width="100%">
                                    <thead>
                                        <tr>
                                            <th>Nombres</th>
                                            <th>Apellidos</th>
                                            <th>DNI</th>
                                            <th>Rol</th>
                                            <th>Activaciones</th>
                                        </tr>                                       
                                    </thead>
                                    <tbody>                                        
                                        <tr ng-repeat="c in g.collaborator">
                                            <th>{{c.user.name}}</th>
                                            <th>{{c.user.last_name}}</th>
                                            <th>{{c.user.dni}}</th>
                                            <th>{{c.rol}}</th>
                                            <th rowspan="100%" width="35%">                                               
                                                <div data-alert class="alert-box secondary" ng-if="g.activations.length=='0'||g.activations==null">
                                                    No contamos con activaciones para este grupo.
                                                </div>
                                                <ul ng-show="g.activations.length!='0'">
                                                    <li ng-repeat="a in g.activations">
                                                        <span data-tooltip aria-haspopup="true" class="has-tip" title="{{a.address}}"><label class="font-normal">{{a.sub_canal}}</label></span>
                                                    </li>
                                                </ul>
                                                
                                                <a href="<?php echo URL::to('/#/{{projectCode}}/group/activation/{{key}}'); ?>" class="button small"><i class="icon-plus"></i> Agregar activación</a>
                                            </th>
                                        </tr>
                                    </tbody>
                                    </table>
                                    <br>
                                </div>
                                
                                <button ng-click="listCollaborator()" data-reveal-id="modalTeam"><i class="icon-plus"></i> Nuevo Equipo</button>
                            </div>
                        </div>                    
                </tab>
                <tab ng-click="select(4)"  active="tabs[4].active" heading="Reportes">
                    <div >
                        <div>
                            <p>
                                Tienes disponible el reporte en dos formatos, usa los botones de abajo para descargarlos.
                            </p>
                            <a href="<?php echo URL::to('api/v1/projects/excel?code={{projectCode}}'); ?>" class="button"><i class="icon-file-excel"></i> Descargar Excel</a>
                            <a href="<?php echo URL::to('api/v1/projects/ppt?code={{projectCode}}'); ?>" class="button"><i class="icon-file-picture"></i> Descargar Power Point</a>
                        </div>
                    </div>
                </tab>
                </div>

			</tabset>
		</div>
	</div>
</div>




