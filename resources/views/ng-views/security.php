<div class="" ng-controller="SecurityController">
	<div class="medium-3 column">
		<div class="block-white content">
		<div class="medium-12 column">
			<div >

			<div class="medium-7 column right" >
				<select  ng-model="formData.roles"  ng-options="r.id as r.display_name for r in filterRoles" ng-change="changeRoles()" >

				</select>
			</div>
				
			</div>
		</div>

		<br>
			<h4>Personas en el sistema</h4>
			<ul class="list-items">
				<li ng-repeat="u in users">
					<div class="list-controls right">
						<a href="#security/{{$index}}"><i class="icon-pencil" style="margin-right: 15px;"></i></a>
					</div>
					<a href="#security/{{$index}}">{{u.name}}</a>
					<span class="list-items-subtitle">{{u.email}}</span>
					<br>
					<span class="text-info">{{u.roles[0].display_name}}</span>
				</li>
			</ul>
			<p>
				<a href="#security/-1" class="button big small-12" ng-click="createUser()"><i class="icon-plus"></i> Agregar persona</a>
			</p>
		</div>
	</div>

	<!-- -->

	<div class=" medium-9 column" ng-hide="userID == null">
		<div class="block-white content" style="overflow:auto;">
			<div class="medium-6 column">
				<div class="content">
					<h3>Datos de la persona</h3>
					<form method="post" id="formUser" name="formUser" ng-submit="saveUser(userID,formUser)">
					<div class="right">
						<button ng-click="statusUser(1)" ng-show="formData.disabled=='0'"><i class="icon-ban"></i> Deshabilitar</button>
						<button ng-click="statusUser(0)" ng-show="formData.disabled=='1'"><i class="icon-check"></i> Habilitar</button>					
					</div>
					<br>

					
						<p><label for="name">Nombre:</label> <input type="text" name="name" ng-model="formData.name" required="" />
                        <div ng-show="formUser.$submitted || formUser.name.$touched">
                            <span ng-show="formUser.name.$error.required">Nombre requerido.</span>
                        </div></p>

                        <p><label for="last_name">Apellido:</label> <input type="text" name="last_name" ng-model="formData.last_name" required=""/>
                        <div ng-show="formUser.$submitted || formUser.last_name.$touched">
                            <span ng-show="formUser.last_name.$error.required">Apellido requerido.</span>
                        </div></p>
                        <p><label for="name">DNI:</label> <input type="text" name="dni" pattern="\d+" maxlength="8" maxlength="1" ng-model="formData.dni" required=""/>
                        <div ng-show="formUser.$submitted || formUser.dni.$touched">
                            <span ng-show="formUser.dni.$error.required">DNI requerido.</span>
                        </div></p>
                        <p><label for="email">Email:</label> <input type="email" name="email" ng-model="formData.email" required=""/>
                        <div ng-show="formUser.$submitted || formUser.email.$touched">
                            <span ng-show="formUser.email.$error.required">Email requerido.</span>
                            <span ng-show="formUser.email.$error.email">Email no valido.</span>
                        </div></p>
                        <p ng-show="formData.rol==2"><label for="company">Compañia:</label> <input type="text" name="company" ng-model="formData.company" /></p>
                        <p><label for="email">Contraseña:</label> <input type="password" ng-model="formData.password"/></p>
						<p><label for="rol">Tipo de usuario:</label>
						<select ng-model="formData.rol" class="form-control" ng-options="r.id as r.display_name for r in roles" ng-change="changeRol()" required>

						</select>
						</p>

						<button type="submit"><i class="icon-check"></i> Guardar</button>
					</form>
				</div>
			</div>
            <div class="medium-6 column" ng-show="userID!=-1">
                <div class="content text-center">
                    <h3>Avatar</h3>
                    <p class="th"><img src="{{formData.path}}" alt=""></p>
                    <p><div class="button" ngf-select ngf-change="upload($files)" ngf-multiple="false"><i class="icon-images" ></i> Subir Imagen</div></p>
                </div>
            </div>
		</div>
	</div>
</div>

