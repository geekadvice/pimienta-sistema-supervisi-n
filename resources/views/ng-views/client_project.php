<div ng-controller="ProjectsController" ng-app="app">
    <div class="column medium-4">
		<div class="block-white">
			<div class="content">
				
				<h4>Campañas</h4>
				<ul class="list-items-content">
                    <input id="projectId" type="hidden" value="{{projectCode}}">
					<li ng-show="factory.projects.data.length == 0"><span class="text-info"><i class="icon-info"></i> No hay campañas disponibles</span>
					</li>
					<li ng-repeat="p in factory.projects.data">
						<div class="right toolbox"><a href="#client-projects/{{p.code}}" class="icon-eye"></a></div>
						<span>{{p.name}}</span><br>
						<span class="text-info">Fecha de inicio: {{p.date_start}}</span>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="column medium-8">

		<div class="block-white content" ng-show="editProject != null">
			<a href="#projects" class="right"><i class="icon-close icon16"></i></a>
			<h3>{{editProject.data.name || 'Cargando...'}}</h3>
			<tabset>
				<!-- Detalle de campaña -->
				<tab ng-click="select(0)" active="tabs[0].active" heading="Detalle de campaña">
					<form ng-submit="saveProject()">
						<div class="inner-column clearfix">

							<div class="column medium-6">
								<div class="input-float">
									<label for="name">Nombre de campaña</label>
									<input disabled type="text" name="name" id="name" ng-model="editProject.data.name">
								</div>
								<div class="input-float">
									<label for="data_start">Fecha de inicio</label>
									<input disabled type="text" name="date_start" id="data_start"
										   ng-model="editProject.data.date_start">
								</div>
								<div class="input-float">
									<label for="">Fecha fin</label>
									<input disabled type="text" name="date_end" ng-model="editProject.data.date_end">
								</div>
							</div>

							<!-- segunda columna -->

							<div class="column medium-6">
								<div class="input-float">
									<label for="">Centro de Costos</label>
									<input disabled type="text" name="code" ng-model="editProject.data.code">
								</div>
								<div class="input-float">
									<label for="client_id">Cliente</label>
									<select disabled name="client_id" id="client_id"
											ng-model="editProject.data.client_id">
										<option value="1">Client</option>
									</select>
								</div>
								<div class="input-float">
									<label for="producer_id">Productor</label>
									<select disabled name="producer_id" id="producer_id"
											ng-model="editProject.data.producer_id">
										<option value="1">Productor</option>
									</select>
								</div>
							</div>
						</div>
						<div class="input-float">
							<label for="">Información de la campaña</label>
							<textarea disabled name="description" id="description" rows="5"
									  ng-model="editProject.data.description"></textarea>
						</div>

						<!-- opciones adicionales -->
						<div class="inner-column clearfix">
							<div class="column medium-6">
								<div class="input-float">
									<label for="status">Estado de la campaña</label>
									<select name="status" id="status" ng-model="editProject.data.status" disabled>
										<option value="0">Pendiente</option>
										<option value="1">Activo</option>
										<option value="2">Cerrada</option>
									</select>
								</div>
							</div>
							<div class="column medium-6">
							</div>
						</div>
					</form>
				</tab>

				<!-- End tab detalle campaña-->
               
                               
                <tab ng-click="select(3)"  active="tabs[3].active" heading="Reportes">
                    <div >
                        <div class="column medium-8">
                            <div class="block-white content" >
                                <p>
                                    Tienes disponible el reporte en dos formatos, usa los botones de abajo para descargarlos.
                                </p>
                                <a href="<?php echo URL::to('api/v1/projects/excel?code={{projectCode}}'); ?>" class="button"><i class="icon-file-excel"></i> Descargar Excel</a>
                                <a href="<?php echo URL::to('api/v1/projects/ppt?code={{projectCode}}'); ?>" class="button"><i class="icon-file-picture"></i> Descargar Power Point</a>
                            </div>
                        </div>
                    </div>
                </tab>
			</tabset>
		</div>
	</div>
</div>




