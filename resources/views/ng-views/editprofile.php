<div ng-controller="editprofileController">

<!-- -->

<div class=" medium-9 column" >
    <div class="block-white content" style="overflow:auto;">
        <div class="medium-6 column">
            <div class="content">
                <h3>Datos de la persona</h3>

                    <br>
                    <p><label for="name">Nombre:</label> <input id="name" name="name"type="text" value="<?php echo \Auth::user()->name?>" /></p>
                    <p><label for="last_name">Apellido:</label> <input id="last_name" name="last_name" type="text" value="<?php echo \Auth::user()->last_name?>"/></p>
                    <p><label for="dni">DNI:</label> <input id="dni" type="text" name="dni" value="<?php echo \Auth::user()->dni?>"/></p>
                    <p><label for="email">Email:</label> <input id="email" name="name" type="text" value="<?php echo \Auth::user()->email?>"/></p>
                    <p ><label for="password">Contraseña:</label> <input type="password" name="password" id="password"/></p>
                 <a ng-click="UpdateUser()" class=" button"><i class="icon-check"></i> Guardar</a>

            </div>

        </div>
        <div class="medium-6 column">
            <div class="content text-center">
                <h3>Avatar</h3>
                <p class="th"><img id="path" src="<?php echo URL::to(\Auth::user()->path) ?>" alt=""></p>
                <p><div class="button" ngf-select ngf-change="upload($files)" ngf-multiple="false"><i class="icon-images" ></i> Subir Imagen</div></p>
            </div>
        </div>

    </div>
</div>
</div>