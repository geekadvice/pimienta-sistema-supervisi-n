<div ng-controller="ReportsController">

    <div id="modalImage" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
      <h4 id="modalTitle" class="text-center">Vista Previa</h4>
        <ul class="related_post_gallery wp_rp">
            <div class="text-center">
                 <img src="<?php echo URL::to ('/') ?>/{{preview}}">
            </div>
        </ul>
    </div>

    <div class="column medium-4">
		<div class="block-white">
			<div class="content">
				<h4>Campañas</h4>
				<ul class="list-items-content">
					<li ng-show="projects.data.length == 0"><span class="text-info"><i class="icon-info"></i> No hay campañas disponibles</span></li>
					<li ng-repeat="p in projects.data">						
						<a ng-click="selectProject(p)">{{p.name}}</a><br>
						<span class="text-info">Fecha de inicio: {{p.date_start}}</span>
                        <div class="right">
                            <span ng-show="p.status==0" class="label">Activo</span>
                            <span ng-show="p.status==1"  class="success warning label">Pendiente</span>
                            <span ng-show="p.status==2"  class="success alert secondary round radius label">Cerrado</span>
                        </div>
					</li>
				</ul>
			</div>
		</div>
	</div>

    <div class="column medium-8 no-left-margin" ng-show="selectedProject!=null">
		<div class="block-white content">
			<a href="#projects" class="right"><i class="icon-close icon16"></i></a>

            <div class="row no-left-margin">
                <div class="column medium-4 no-left-margin">
                        <h4>Activaciones</h4>
                    <ul class="list-items-content">
                           <li ng-show="activations.length == 0"><span class="text-info"><i class="icon-info"></i> No hay activaciones disponibles</span></li>
                            <li ng-repeat="activation in activations">
                                <a ng-click="selectactivation(activation.id,activation.status)" >{{activation.sub_canal}}</a><br>
                                <span class="text-info">Fecha: {{activation.date}}</span>
                                <div class="right">
                                    <span ng-show="activation.status==0" class="label">Activo</span>
                                    <span ng-show="activation.status==1"  class="success warning  label">Pendiente</span>
                                    <span ng-show="activation.status==2"  class="success alert label">Cerrado</span>
                                </div>
                            </li>
                    </ul>
                </div>
                <br>
                <div  ng-show="activation_id!=0"class="column medium-7" style="margin-left: 40px;">
                    <br>
                    <div data-alert class="alert-box secondary" ng-show="indicators.length==0">
                        No se encontraron indicadores.
                    </div>

                    <div ng-show="indicators.length!=0" >
                        <h5>Indicadores</h5>
                    <ul>
                    <div ng-repeat="i in ind">
                        <h3 ng-if="ind[$index-1].group!=i.group" >{{i.group}}</h3>
                        <div ng-if="i.type=='booleano'">   
                            <label>{{i.name}}</label>
                            <!--  <span ng-repeat="o in options">{{o}}
                                <input name="{{i.id}}" id="{{i.id}}" type="radio" ng-click="select(i.id,options.value)" ng-value="options" ng-model="i.value">-->

                            <!--  </span>-->
                            <input ng-disabled="!enable_activation" ng-model="i.value" name="{{i.id}}" id="{{i.id}}" type="radio" value="Si"> Si
                            <input ng-disabled="!enable_activation" ng-model="i.value" name="{{i.id}}" id="{{i.id}}" type="radio" value="No"> No
                        </div>

                        <div ng-if="i.type=='cualitativo'">
                            <label>{{i.name}}</label>
                            <select ng-model="i.value" ng-disabled="!enable_activation">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>

                            <!--<pagination direction-links="false"  oundary-links="false" total-items="50" ng-change="pageChanged()" ng-model="formData.cuantitativo"
                                    max-size="5" items-per-page="10" class="pagination">
                          </pagination>-->
                        </div>

                        <div ng-if="i.type=='cuantitativo'">
                            <label>{{i.name}}</label>
                            <input ng-model="i.value" type="number" ng-disabled="!enable_activation" only-digits >
                        </div>

                        <div ng-if="i.type=='libre'">
                            <label>{{i.name}}</label>
                            <textarea ng-model="i.value" ng-disabled="!enable_activation"></textarea>
                        </div>

                        <div ng-if="i.type=='imagen' ">                            
                            <label>{{i.name}}</label>                            
                            <div class="ajax-upload-dragdrop" style="vertical-align: top; width: 500px;">
                                <div class="ajax-file-upload" style="position: relative; overflow: hidden; cursor: default;">                                    

                                    <span  ng-show="i.path.length=='0'||i.path==null" class="text-info"><i class="icon-info"></i> No se encontraron imagenes.</span>
                                    <ul id="gallery" class="related_post_gallery wp_rp">
                                        <li ng-repeat="img in i.path" data-position="0" data-poid="in-1395" data-post-type="none">
                                            <img data-reveal-id="modalImage" ng-click="previewImage(img.path)" name='{{img.name}}' width="80px" height="80px" class="image_gallery" src="{{img.path}}" />
                                            <div style="line-height: 10px;margin: 5px 0;">
                                                <a ng-show="enable_activation==true" ng-click="deleteImage(img.id)" class="delete_image">Eliminar</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <br>
                            <div ng-show="enable_activation==true" class="button" ngf-select ngf-change="upload($files,i.id)" ngf-multiple="multiple"><i class="icon-images" ></i> Subir Imagen</div>
                        </div>
                    </div>
                    </ul>
                    <br>
                    <div class="text-center" ng-show="enable_activation==true">
                        <a class="button" ng-click="saveIndicator()"><i class="icon-check" ></i> Guardar Avance</a>
                        <a class="button" ng-click="generateReport()"><i class="icon-doc" ></i> Generar Reporte</a>
                    </div>
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>




