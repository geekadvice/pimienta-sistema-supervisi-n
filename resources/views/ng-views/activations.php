<div ng-controller="ActivationController" ng-app="app">
   <div class="column large-12">
		<div class="block-white">
			<div class="content">
                <a href="<?php echo URL::to('/#/projects/{{projectCode}}'); ?>" style="text-decoration: underline"><< Atras</a>
                <br>
                <br>

            <h3>Activaciones</h3>
            <table width="100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Local</th>
                        <th>Dirección</th>
                        <th>Referencia</th>
                        <th>Distrito</th>
                        <th>Cliente</th>
                        <th>Razón Social</th>
                        <th>Hora de inicio</th>
                        <th>Hora de fin</th>
                        <th>Supervisor</th>
                        <th>Persona de contacto POS</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in activationsfree.data">
                        <th><input ng-model="item.check" ng-checked="item.check" type="checkbox"></th>
                        <th>{{item.sub_canal}}</th>
                        <th>{{item.address}}</th>
                        <th>{{item.reference}}</th>
                        <th>{{item.district}}</th>
                        <th>{{item.client}}</th>
                        <th>{{item.company}}</th>
                        <th>{{item.h_start}}</th>
                        <th>{{item.h_end}}</th>
                        <th>{{item.supervisor}}</th>
                        <th>{{item.agent_comercial}}</th>
                    </tr>
                </tbody>
            </table>

            <div class="large-12 columns" >
                <div class="right">
                     <button ng-click="actualizar()"><i class="icon-refresh"></i> Actualizar</button>
                    <button ng-click="asignaractivacion()"><i class="icon-check"></i> Asignar</button>
                </div>
            </div>
            <br>
            <br>
                <br>
            <div class="row content-map">
                <div class="large-12 columns">
                    <div id="map-canvas"></div>
                </div>
            </div>


        </div>
        </div>
    </div>
</div>