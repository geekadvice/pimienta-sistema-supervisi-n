@extends('app')

@section('content')
	<div class="medium-6 small-centered column">
		<h1>Error 404</h1>
		<p>No pudimos encontrar nada en esta url, por favor ve a home para seguir navegando</p>
	</div>
@endsection