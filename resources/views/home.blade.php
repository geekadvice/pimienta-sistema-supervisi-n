@extends('app')

@section('content')
	<div>
		<div id="main-menu" ng-controller="MenuController">
			<div class="text-center main-menu-logo">
				<img src="{{ asset('img/logo.png') }}" width="65" alt="Logo"/>
			</div>
			<ul>
				<li ng-repeat="s in sections">
					<a tabindex="0" href="@{{s.href}}" ng-class="isSelected(s)"><i class="@{{s.icon}} main-menu-icon"></i><br/>@{{s.label}}</a>
				</li>
			</ul>
		</div>

		<!-- End main menu-->
		<div id="content">
			<div class="content-topbar">
				<div class="right toolbox">
					<span href="#profile">Hola {{ Auth::user()->name }}</span>
					<a href="{{ url('auth/logout') }}"><i class="icon-power"></i></a>
					<a zf-hard-toggle="separate-actionsheet"   data-dropdown="drop1" aria-controls="drop1" class="icon-dots-three-vertical"></a>

					<ul id="drop1"  data-dropdown-content class="f-dropdown" aria-hidden="true">
                      <li><a href="#editprofile">Editar Perfil</a></li>
                    </ul>


				</div>

				<div class="toolbox hidden-for-small">
					<label for="search" class="icon-search">
						<i class="icon-magnifier"></i>
					</label>
					<input class="hidden-input" name="search" id="search" type="text" placeholder="Haz click para buscar"/>
				</div>
				<div class="show-for-small-only">
					<img src="{{ asset('img/logo.png') }}" width="65" alt="Logo"/>
				</div>
			</div>
			<!-- End top bar -->
			<div class="content-section">
				<div ng-view></div>
			</div>
		</div>
	</div>
@endsection
