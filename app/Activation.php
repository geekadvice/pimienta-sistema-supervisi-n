<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Activation extends Model
{

	protected $fillable = ['name', 'code_sap', 'code_local', 'project_id','lat','lon','h_start','h_end','company','address','district','reference','supervisor','supervisor_phone','agent_comercial','agent_phone','observation'];

	function isValid($input)
	{
		$rules = array('name' => 'required', 'address' => 'required');
		$validator = Validator::make ($input, $rules);
		return $validator;
	}

	public function project()
	{
		return $this->belongsTo('App\Project');
	}

	public function team()
	{
		return $this->belongsToMany('App\User')->withPivot('type')->withTimestamps();
	}

	public function reports()
	{
		return $this->hasMany('App\Report');
	}

	public function getSupervisor()
	{
		return $this->team()->where('type', 'supervisor')->first();
	}

    public function process()
    {
        return $this->hasMany('App\Process');
    }
    public function group()
    {
        return $this->belongsToMany('App\Activation','team_activation')
            ->withPivot('team_id','activation_id');
    }
    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }
}
