<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//If are public view
// Route::get('/', 'WelcomeController@index');
//If are not public view
Route::get('/', 'HomeController@index');

Route::controller('home', 'HomeController');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['prefix' => 'api/v1'], function()
{
    Route::controller('users', 'Api\ApiUsersController');
    Route::controller('projects', 'Api\ApiProjectController');
    Route::controller('indicators', 'Api\ApiIndicatorController');
    Route::controller('activations', 'Api\ApiActivatorController');
    Route::controller('images', 'Api\ApiImageController');
    Route::controller('teams', 'Api\ApiTeamController');
});
// Opcionales
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
