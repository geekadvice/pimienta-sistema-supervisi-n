<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\User;
use  App\Commands\Command;
class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

        $d=new Command();
        $s=$d->disabled();
       if($s==1)
        {
            return view('auth.login');
        }
		return view('home');
	}

	public function getMail()
	{
		/*Mail::send('emails.nike', ['key' => 'value'], function($message)
		{
			$message->to('pentarunners@nikelima.com', 'Nike')->subject('Publicidad - ¿Estás listo para seguir mejorando?');
			$message->attach('PentaRunners Control 5K.pdf');
		});*/
	}

	public function anyDashboard()
	{
        $d=new Command();
        $s=$d->disabled();

        if($s==1)
        {
            return view('auth.login');
        }
		return view('ng-views.dashboard');
	}

	public function anyProjects()
	{
        $d=new Command();
        $s=$d->disabled();

        if($s==1)
        {
            return view('auth.login');
        }
		return view('ng-views.projects');
	}

    public function anyActivations()
    {
        $d=new Command();
        $s=$d->disabled();

        if($s==1)
        {
            return view('auth.login');
        }
        return view('ng-views.activations');
    }

    public function anyActivationsGroup()
    {
        $d=new Command();
        $s=$d->disabled();

        if($s==1)
        {
            return view('auth.login');
        }
        return view('ng-views.group_activation');
    }

	public function anyProfile()
	{
        $d=new Command();
        $s=$d->disabled();

        if($s==1)
        {
            return view('auth.login');
        }
		return view('ng-views.profile');
	}

	public function anyReports()
	{
        $d=new Command();
        $s=$d->disabled();

        if($s==1)
        {
            return view('auth.login');
        }
		return view('ng-views.reports');
	}

	public function anyConfig()
	{
        $d=new Command();
        $s=$d->disabled();

        if($s==1)
        {
            return view('auth.login');
        }
		return view('ng-views.config');
	}

	public function anyTeam()
	{
        $d=new Command();
        $s=$d->disabled();

        if($s==1)
        {
            return view('auth.login');
        }
		return view('ng-views.team');
	}

	public function anySecurity()
	{
        $d=new Command();
        $s=$d->disabled();

        if($s==1)
        {
            return view('auth.login');
        }
		return view('ng-views.security');
	}
    public function anyEditprofile()
    {
        $d=new Command();
        $s=$d->disabled();
        if($s==1)
        {
            return view('auth.login');
        }
        return view('ng-views.editprofile');
    }

	public function anyClientprojects()
	{
        $d=new Command();
        $s=$d->disabled();
        if($s==1)
        {
            return view('auth.login');
        }
		return view('ng-views.client_project');
	}

}
