<?php namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use DB;
use App\User;
use App\Role;
use App\Project;
use App\Team;
use Validator;
//use Illuminate\Database\Query\Builder;
class ApiUsersController extends ApiController {

	public function __construct()
	{
		//$this->apiModel = 'App\User';
        parent::__construct('App\User');
	}

    public function anyFilterrole()
    {
        if(Input::has('role'))
        {
            if(Input::get ('role')=='0')
            {
                $entries = User::with('roles')->get();
            }
            else
            {
                $entries = User::whereHas ('roles', function ($q) {
                            $q->where ('role_id', '=', Input::get ('role'));
                        })->with('roles')->get ();
            }
            
            $this->apiResponse->data=$entries;
        } 
        else
        {
            $this->apiResponse->description='Faltan parametros';
        }
       
        return response()->json($this->apiResponse);
    }

    public function anyStatus()
    {
        if(Input::has('user_id')&&Input::has('status'))
        {
            $user = User::find(Input::get('user_id'));
            if(isset($user))
            {
                $user->disabled=Input::get('status');
                $user->save();
            }
        } 
        else
        {
            $this->apiResponse->status->setStatus(Status::STATUS_ERROR_PROCESO);
        }
       
        return response()->json($this->apiResponse);
    }

	public function anyGetall($_code = null)
	{

		$users = User::with('roles')->get();
		$roles = Role::all();
		$this->apiResponse->data['users'] = $users;
		$this->apiResponse->data['roles'] = $roles;
		return response()->json($this->apiResponse);
	}

    public  function anyCurrent()
    {
        $users=\Auth::user()->load('roles');
        if(isset($users)){
            $this->apiResponse->data = $users;
        }
        return response()->json($this->apiResponse);
    }

	public function anySave($_code = null)
	{
		$data = Input::all();
		DB::beginTransaction();
		try {
			if(Input::has('personid'))
			{
				$user = User::find(Input::get('personid'));
			}
			else 
			{
				$user = new User();
			}
			if(isset($data['password'])) $data['password'] = Hash::make($data['password']);
			$user->fill($data);
            if(!isset($user->path)){$user->path = 'img/avatar/001.png';}

			$user->save();
			if(Input::has('rol')) $user->roles()->sync(array(Input::get('rol')));
			
			$this->apiResponse->data['user'] = $user;
			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			$this->apiResponse->status->setStatus(Status::STATUS_ERROR_PROCESO);
		}
		

		return response()->json($this->apiResponse);
	}
    public function anyFind()
    {
        if(Input::has('search'))
        {
            $code=Input::get('code');
            $users = User::whereHas('roles' , function($query)
            {
                $query->where('id', '4');
            })->whereDoesntHave('projects' , function($query) use($code)
            {
                $query->where('code', $code);
            })->where('name', 'like', '%'.Input::get('search').'%')->get();

            $this->apiResponse->data=$users;
        }
        return response()->json($this->apiResponse);
    }
    public function anyFinduser()
    {
        if(Input::has('search'))
        {
            $users = User::whereHas('roles' , function($query)
            {
                $query->where('id', Input::get('rol'));
            })->where('name', 'like', '%'.Input::get('search').'%')->get();
            $this->apiResponse->data=$users;
        }
        return response()->json($this->apiResponse);
    }
    public function anyFindCollaborator()
    {
        $number=Input::get('number');
        if(Input::has('search'))
        {
            $project=Project::where('code',Input::get('code'))->first();
            $users = User::whereHas('roles' , function($query)
            {
                $query->where('id', '5');
            })->whereDoesntHave('team' , function($query) use($number,$project)
            {

                $query->where('number', $number);
                $query->where('project_id', $project->id);
            })->where('name', 'like', '%'.Input::get('search').'%')->get();
        }
        $this->apiResponse->data=$users;
        return response()->json($this->apiResponse);
    }
    public function anyAdduserproyect()
    {
        $user_id=Input::get('user_id');
        $code=Input::get('code_project');
        if(isset($user_id) && isset($code))
        {
            $project=Project::where('code',$code)->first();

            $user=User::find($user_id);
            if (!$user->projects->contains($project->id)) {
                $user->projects()->attach($project->id);
                $this->apiResponse->description='se agrego correctamente';
            }
            else
            {
                $this->apiResponse->description='el supervisor ya se agrego anteriormente';
            }

        }
        else
        {
            $this->apiResponse->description='no se selecciono ningun supervisor';
        }
        return response()->json($this->apiResponse);
    }
    public function anyListsupervisorproject()
    {

        $users='';
        if(Input::has('code'))
        {
            $users = User::whereHas('projects' , function($query)
            {
                $code=Input::get('code');
                $query->where('code',$code );
            })->get();
            foreach($users as $user)
            {
                $numero=DB::table('users')
                    ->join('activation_user', 'users.id', '=', 'activation_user.user_id')
                    ->where('users.id','=',$user->id)
                    ->count();
                $user->carga=$numero;
            }
        }
        $this->apiResponse->data=$users;
        return response()->json($this->apiResponse);
    }
    public function anyUpdateuser()
    {
        $data=Input::all();
        if(isset($data['password'])) $data['password'] = Hash::make($data['password']);
        $user=User::find(\Auth::user()->id);
        $validator=$user->isValid($data);
        if($validator->passes())
        {
            $user->fill($data);
            $user->save();
            $this->apiResponse->data=$user;
        }
        else
        {
            $this->apiResponse->code='220';
            $this->apiResponse->description='faltan campos';
        }
        return response ()->json($this->apiResponse);
    }
    public function anyImage()
    {
        if(Input::has('userid'))
        {
            $id=Input::get('userid');
        }
        else
        {
            $id=\Auth::user()->id;
        }
        $image = Input::file ('file');
        $name = $image->getClientOriginalName();
        $destinationPath = 'img/uploads';
        $extension = $image->getClientOriginalExtension();
        $fileName =rand (1111111111, 9999999999) . '.' . $extension;
        $file = $destinationPath.'/'.$fileName;
        $destinationPath=public_path().'/'.$destinationPath;
        $image->move ($destinationPath, $fileName);

        if(isset($file))
        {
            $user=User::find($id);
            if(isset($user->id))
            {
                $user->path=$file;
                $user->save();
            }
            $this->apiResponse->data=$user;
        }
        return response ()->json($this->apiResponse);
    }



}
