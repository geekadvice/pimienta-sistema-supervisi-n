<?php namespace App\Http\Controllers\Api;

use App\Indicator;
use App\Project;
use App\Report;
use App\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;

class ApiIndicatorController extends ApiController {

    public function __construct()
    {
        parent::__construct('App\Indicator');
    }

     public function anyImage()
    {

        $data = Input::all();
        //return $data;      
        try{

            DB::beginTransaction();
            $image = Input::file ('file');
            $name = $image->getClientOriginalName();

            $destinationPath = 'img/uploads';
            $extension = $image->getClientOriginalExtension();
            $fileName =rand (1111111111, 9999999999) . '.' . $extension;
            $file = $destinationPath.'/'.$fileName;
            $destinationPath=public_path().'/'.$destinationPath;
            $image->move ($destinationPath, $fileName);
            

            if(isset($file)&&Input::has('activation_id')&&Input::has('indicator_id'))
            {
                $report=Report::where('activation_id',Input::get('activation_id'))->where('indicator_id',Input::get('indicator_id'))->first();
                if(count($report)==0){
                    $report=new Report();
                }
                    $report->activation_id=Input::get('activation_id');
                    $report->indicator_id=Input::get('indicator_id');
                    $report->user_id=\Auth::user()->id;
                    $report->value='imagen';                    
                    $report->save();
                    
                    $name = $image->getClientOriginalName();                                                
                    $images=new Image();
                    $images->name=$name;
                    $images->imageable_id=$report->id;
                    $images->imageable_type='App\Report';
                    $images->path=$file;
                    $images->save();
                        
                    }
                    DB::commit();
                    return response ()->json ($this->apiResponse);                    
                }
            catch (Exception $e) {
                if(isset($file))
                {
                    File::delete($destinationPath.$fileName);
                }
                DB::rollback();
                $this->apiResponse->status->setStatus(Status::STATUS_ERROR_PROCESO);
            }        
    }
    public function anyDeleteindicator()
    {
        if(Input::has('id'))
        {
            $id=Input::get('id');
            $indicator=Indicator::find($id);
            if($indicator->type=='imagen')
            {
                $reports=  Report::where('indicator_id',$indicator->id)->get();

                foreach($reports as $report)
                {
                    $imgs = Image::where('imageable_id', $report->id)->where('imageable_type', 'App\Report')->get();
                    foreach ($imgs as $img) {

                        if (isset($img->path)) {
                            if (file_exists('../public/' . $img->path)) unlink('../public/' . $img->path);
                        }
                    }
                    Image::where('imageable_id', $report->id)->where('imageable_type', 'App\Report')->delete();
                }
            }
            Report::where('indicator_id',$indicator->id)->delete();
            $indicator->delete();
        }

    }

}