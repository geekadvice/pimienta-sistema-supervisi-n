<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Middleware\ApiResponse;
use App\Http\Middleware\Status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use PhpSpec\Exception\Exception;
use Symfony\Component\Debug\Exception\FatalErrorException;

class ApiController extends Controller {

	private $className;
	public $apiModel;
	public $apiResponse;

	function __construct($_model)
	{
		$this->className = $_model;
		try{

			if(class_exists($_model))
			{
				$this->apiResponse = new ApiResponse();
				$this->apiModel = new $_model;
			}
		}catch (FatalErrorException $e)
		{
			echo $e->getMessage();
		}
	}

	public function checkApi()
	{
		return true;
	}

	public function getIndex()
	{
		$html = '<h1>API status</h1>';
		$html .= '<p>Model: ' . $this->className . ' <b>' . ($this->apiModel?'OK':'La clase no existe') . '</b></p>';
		if($this->apiModel)
		{
			$html .= '<p>Métodos disponibles:</p><pre>';
			$html .= print_r(get_class_methods($this), true);
			$html .= '</pre>';
		}

		return $html;
	}

	public function anySelect($_id = null)
	{
		if($_id != null)
		{
			$entries = $this->apiModel->where('id', $_id)->get();
		}else{
			$entries = $this->apiModel->get();
		}

		$this->apiResponse->data = $entries;

		return response()->json($this->apiResponse);
	}

	public function anySave($_id = null)
	{
		$this->apiResponse = new ApiResponse();

		$data = Input::all();

		if(isset($data['id']))
		{
			$entrie = $this->apiModel->find($data['id']);
		}else{
			$entrie = $this->apiModel;
		}

		$entrie->fill($data);
		$entrie->save();

		$this->apiResponse->setData($entrie);

		return response()->json($this->apiResponse);
	}

	public function anyDelete($_id = null)
	{
		$this->apiResponse = new ApiResponse();

		$id = null;

		if($_id != null) $id = $_id;
		else if(Input::has('id')){
			$id = Input::get('id');
		}

		if($id)
            $model=$this->apiModel->where('id', $id)->first();
        if(isset($model->path)){
            if (file_exists('../public/'.$model->path)) unlink('../public/'.$model->path);
        }
			$deleted = $this->apiModel->where('id', $id)->delete();
		if($deleted)
			$this->apiResponse->setData($deleted);
		else
			$this->apiResponse->status->setStatus(Status::STATUS_ERROR_PARAMETROS);

		return response()->json($this->apiResponse);
	}


}
