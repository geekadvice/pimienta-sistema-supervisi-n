<?php namespace App\Http\Controllers\Api;


use App\Image;
use App\Project;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;


class ApiImageController extends ApiController
{
    public function __construct ()
    {
        parent::__construct ('App\Image');
    }

    public function anyList()
    {
        if(input::has('activation_id')) {

           $image=Image::where('imageable_type','App\Activation')->where('imageable_id',input::get('activation_id'))->get();
            $this->apiResponse->data=$image;
        }
        else{
            $this->apiResponse->status->description='Faltan parametros';
            $this->apiResponse->status->code='220';
        }
        return response()->json($this->apiResponse);
    }

}