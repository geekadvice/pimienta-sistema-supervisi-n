<?php namespace App\Http\Controllers\Api;

use App\Activation;
use App\Image;
use App\Project;
use App\Process as proce;
use App\Report;
use App\Team;
use App\Indicator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use Validator;
use Illuminate\Support\Facades\File;



class ApiActivatorController extends ApiController {
    public function __construct()
    {
        parent::__construct('App\Activation');
    }

    public function anyGroups()
    {
        if(Input::has('project_code')&&Input::has('number'))
        {
            $project=Project::where('code',Input::get('project_code'))->first();


            if(isset($project))
            {
                $team=Team::with('activations')->where('project_id',$project->id)->where('number',Input::get('number'))->first();
                if(isset($team))
                {
                    $activations=Activation::where('project_id',$project->id)->get();
                    $activation_id=DB::table('team_activation')->where('team_id',$team->id)->lists('activation_id');
                }
                $this->apiResponse->data=(array('data'=>$activations,'activations_id'=>$activation_id));
            }
        }
        return response ()->json ($this->apiResponse);
    }

    public function  anyGroupAssignation()
    {
        if(Input::has('team_number')&&Input::has('activations_id')&&Input::has('project_code'))
        {

            $project=Project::where('code',Input::get('project_code'))->first();
            if(isset($project))
            {
                $team_id=Team::where('number',Input::get('team_number'))->where('project_id',$project->id)->lists('id');
                $activation_delete=Activation::where('project_id',$project->id)->lists('id');

                $activations_id=Input::get('activations_id');

                if(isset($activations_id)!=0)
                {

                    $team=Team::find($team_id);
                    DB::table('team_activation')->whereIn('team_id',$team_id)->whereIn('activation_id',$activation_delete)->delete();
                    foreach($activations_id as $valactivations_id)
                    {
                        foreach($team as  $valteam)
                        {
                            $valteam->activations()->attach($valactivations_id);
                        }

                    }
                }
            }
        }

        return response ()->json ($this->apiResponse);
    }

    public function anyFind()
    {
        if(Input::has('activation_id'))
        {
            $activation=Activation::find(Input::get('activation_id'));
            if(isset($activation))
            {
                $activation->load('images');
                $this->apiResponse->data=$activation;
            }
            else{
                $this->apiResponse->status->description='No se encontraron registros';
            }
       }
       return response ()->json ($this->apiResponse);
    }


    public function anyImage()
    {
        $image = Input::file ('file');
        $name = $image->getClientOriginalName();

        $destinationPath = 'img/uploads';
        $extension = $image->getClientOriginalExtension();
        $fileName =rand (1111111111, 9999999999) . '.' . $extension;
        $file = $destinationPath.'/'.$fileName;
        $destinationPath=public_path().'/'.$destinationPath;
        $image->move ($destinationPath, $fileName);

        if(isset($file)&&Input::has('activation_id'))
        {
            $activation=Activation::find(Input::get('activation_id'));
            if(isset($activation->id))
            {
                $images=new Image();
                $images->name=$name;
                $images->path=$file;
                $images->imageable_type='App\Activation';
                $images->imageable_id=$activation->id;
                $images->save();
            }
        }
        return response ()->json ($this->apiResponse);
    }

    public function anySaveObservation()
    {
        if(Input::has('activation_id')&&Input::has('observation')) {
            $activation = Activation::find (Input::get ('activation_id'));
            if (isset($activation->id)) {
                $activation->observation=Input::get('observation');
                $activation->save();
            }
        }

        return response ()->json ($this->apiResponse);
    }

    public function anyLoadexcel()
    {
        $xls=null;
        $result=null;
        $data=Input::all();

        if(Input::has('code_project')){
            if(Input::hasFile('file'))
            {

                $destinationPath = 'xls';
                $extension = Input::file ('file')->getClientOriginalExtension();
                $fileName =rand (1111111111, 9999999999) . '.' . $extension;
                Input::file ('file')->move ($destinationPath, $fileName);
                $file = $destinationPath.'/'.$fileName;

                $result=\Excel::load($file, function($reader) {
                    $reader->toObject();
                    $reader->formatDates(false);
                })->get();

                $project=Project::where('code',Input::get('code_project'))->get();
                foreach ($result as $key => $row) {
                    $activation=new Activation();
                    if(count($project)!=0) {

                        $activation->project_id = $project[0]->id;
                        $activation->week = $row->semana;
                        $activation->area = $row->gerencia;
                        $activation->date = $row->fecha;
                        $activation->code_sap = $row->codigo_sap;
                        //$activation->code_local = $row->codigo_del_local;
                        //$activation->h_start = $row->hora_de_inicio;
                        //$activation->h_end = $row->hora_de_fin;
                        $activation->lat = $row->latitud;
                        $activation->lon = $row->longitud;
                        $activation->client = $row->cliente;
                        $activation->company = $row->razon_comercial;
                        $activation->address = $row->direccion;
                        $activation->district = $row->distrito;
                        $activation->reference = $row->referencia;
                        $activation->supervisor = $row->supervisor;
                        $activation->supervisor_phone = $row->celular_supervisor;
                        $activation->agent_comercial = $row->agente_comercial;
                        $activation->agent_phone = $row->celular_agente;
                        $activation->contact_pos = $row->persona_de_contacto_pos;
                        $activation->sub_canal = $row->sub_canal;
                        $activation->sub_canalv2 = $row->sub_canalv2;
                        $activation->status=0;
                        $activation->save ();
                    }
                }
                $this->apiResponse->data=$activation;
                if(isset($file))File::delete($file);
            }
            else{
                $this->apiResponse->status->code="220";
                $this->apiResponse->status->details= "No se ha seleccionado ningun archivo excel.";
            }
        }
        return response()->json($this->apiResponse);
    }
    //lista de activaciones sin supervisor
    public function anyListactivacionesfree()
    {
        if(Input::has('code') )
        {
           // $project=Project::where('code',Input::get('code'))->first();
            //SELECT  `id`,  `project_id`,  `week`,  `area`,  `status`,  `code_sap`,  `code_local`,  `contact_pos`,  `type`,  `lat`,  `lon`,  `date`,  `h_start`,  `h_end`,  `sub_canal`,  `sub_canalv2`,  `client`,  `company`,  `address`,  `district`,  `reference`,  `supervisor`,  `supervisor_phone`,  `agent_comercial`,  `agent_phone`,

            $activaciones=DB::Table('activations')
              //  ->join('projects','activations.project_id','=','projects.id')
                ->join('projects', function ($join) {
                   $project=Project::where('code',Input::get('code'))->first();
                    $join->on('activations.project_id', '=', 'projects.id')
                        ->where('activations.project_id', '=', $project->id);
                })
                //->join('activation_user','activations.id','=','activation_user.activation_id')
                ->leftJoin('activation_user','activations.id','=','activation_user.activation_id')
                ->orWhere('activation_user.user_id','=',Input::get('user_id'))
                ->orWhereNull('activation_user.activation_id')
               // ->where('projects.id','=',$project->id)
                ->select('activations.id','activations.area','activations.code_sap','activations.code_local','activations.contact_pos','activations.type','activations.lat','activations.lon','activations.date','activations.h_start','activations.h_end','activations.sub_canal','activations.sub_canalv2','activations.client','activations.company','activations.address','activations.district','activations.reference',  'activations.supervisor',  'activations.supervisor_phone',  'activations.agent_comercial',  'activations.agent_phone','activation_user.activation_id'  )
                ->orderBy('activations.lat', 'desc')
                ->get();
              foreach($activaciones as $a)
              {
                  $a->check=false;
                  if($a->activation_id!=null)
                  {
                      $a->check=true;
                  }
                  else{
                      $a->check=false;
                  }

              }
            $this->apiResponse->data=$activaciones;
        }
        else{
            $this->apiResponse->status->code="220";
            $this->apiResponse->status->details= "No se ha seleccionado ninguna campaña";
        }

        return response()->json($this->apiResponse);
    }
    public function anyAssignactivations()
    {
        $activations=Input::get('activations');
        $user_id=Input::get('supervisor_id');
        if(isset($activations))
        {

            foreach($activations as $act)
            {
                if($act['check']==true)
                {
                    DB::table('activation_user')->insert(
                        ['activation_id' => $act['id'], 'user_id' => $user_id,'type'=>'supervisor']
                    );
                }
                else
                {
                    DB::table('activation_user')->where('activation_id', '=', $act['id'])->delete();
                }
            }
        }
        else{
            $this->apiResponse->status->code="220";
            $this->apiResponse->status->details= "No se ha seleccionado ninguna activación";
        }
        return response()->json($this->apiResponse);
    }
    public function anyListactivationscampaing()
    {
        $code=Input::get('code');

        if(Input::has('code'))
        {
            $project=Project::where('code',Input::get('code'))->first();
            $activations=Activation::where('project_id',$project->id)->orderBy('status')->get();
            $indicators=Indicator::where('project_id',$project->id)->orderBy('group','asc')->get();
            $array=[];
                $array['activations']=$activations;
            $array['indicators']=$indicators;
            $this->apiResponse->data=$array;
        }
        return response()->json($this->apiResponse);
    }
    public function anySavereport()
    {
       //$ind= array(['id'=>1,'value'=>'asdasd'],['id'=>2,'value'=>'asdasd'],['id'=>3,'value'=>'asdasd']);
      //  $img= array('asdasd','asdasd','sdfsdf','sdfdsf');
        $indicators=Input::get('indicators');
      //  $indicators=$ind;
        $activation_id=Input::get('activation_id');
        if(isset($activation_id))
        {
           
            foreach( $indicators as $indicator)
            {
                $report=Report::where('activation_id',$activation_id)->where('indicator_id',$indicator['id'])->first();
                if(count($report)==0){
                    $report=new Report();
                    $report->activation_id=$activation_id;
                    $report->indicator_id=$indicator['id'];
                }
                $report->user_id=\Auth::user()->id;
                if(isset($indicator['value']))
                {
                    $report->value=$indicator['value'];
                }
                $report->date=date("Y-m-d H:i:s");
                $report->hour=date("H:i:s");
                $report->save();
                $indicador=Indicator::find($indicator['id']);
            }
            $act=Activation::find($activation_id);
            $act->status=2;
            $act->save();
        }
        else{
            $this->apiResponse->status=220;
            $this->apiResponse->description='faltan parametros';
        }
        return response()->json($this->apiResponse);
    }
    public function anySavereportpreview()
    {
        //$ind= array(['id'=>1,'value'=>'asdasd'],['id'=>2,'value'=>'asdasd'],['id'=>3,'value'=>'asdasd']);
        //  $img= array('asdasd','asdasd','sdfsdf','sdfdsf');
        $indicators=Input::get('indicators');
        //  $indicators=$ind;
        $activation_id=Input::get('activation_id');
        if(isset($activation_id))
        {

            foreach( $indicators as $indicator)
            {

                $report=Report::where('activation_id',$activation_id)->where('indicator_id',$indicator['id'])->first();
                if(count($report)==0){
                    $report=new Report();
                    $report->activation_id=$activation_id;
                    $report->indicator_id=$indicator['id'];
                }
                $report->user_id=\Auth::user()->id;
                if(isset($indicator['value']))
                {
                    $report->value=$indicator['value'];
                }
                $report->date=date("Y-m-d H:i:s");
                $report->hour=date("H:i:s");
                $report->save();
            }
        }
        else{
            $this->apiResponse->status=220;
            $this->apiResponse->description='faltan parametros';
        }
        return response()->json($this->apiResponse);
    }
    public function anyActivationindicators()
    {
        $activation_id=Input::get('activation_id');
        if(Input::has('activation_id'))
        {
            $report=Report::with('images','activation','indicator')->where('activation_id',$activation_id)->get();
            $this->apiResponse->data= $report;
        }
        else{
            $this->apiResponse->status=220;
            $this->apiResponse->description='faltan parametros';
        }
        return response()->json($this->apiResponse);
    }
    public function anyStatuschange()
    {
        if(Input::has('activation_id') && Input::has('status'))
        {
            $activation=Activation::find(Input::get('activation_id'));
            $activation->status=Input::get('status');
            $activation->save();
        }
        else{
            $this->apiResponse->status=220;
            $this->apiResponse->description='faltan parametros';
        }
        return response()->json($this->apiResponse);
    }
    public function anyDeleteactivation()
    {
        try {


            if (Input::has('activation_id')) {
                $id = Input::get('activation_id');
                $activation = Activation::find($id);

                $imgs = Image::where('imageable_id', $id)->where('imageable_type', 'App\Activation')->get();
                foreach ($imgs as $img) {
                    if (isset($img->path)) {
                        if (file_exists('../public/' . $img->path)) unlink('../public/' . $img->path);
                    }
                }
                Image::where('imageable_id', $id)->where('imageable_type', 'App\Activation')->delete();
                $indicators = Report::with('images')->where('activation_id', $id)->get();
                foreach ($indicators as $i) {
                    if (isset($i->images)) {
                        foreach ($i->images as $img) {
                            if (isset($img->path)) {
                                if (file_exists('../public/' . $img->path)) unlink('../public/' . $img->path);
                            }
                        }
                        Image::where('imageable_id', $i->id)->where('imageable_type', 'App\Report')->delete();
                    }
                }
                Report::where('activation_id', $id)->delete();
                proce::where('activation_id', $id)->delete();

                $activation->team()->detach();
                $activation->group()->detach();
                Activation::destroy($id);
            }
            else
            {
                $this->apiResponse->status->setStatus(Status::STATUS_ERROR_PROCESO);
            }
            DB::commit();
            return response ()->json ($this->apiResponse);
        }   catch (Exception $e) {
            DB::rollback();
            $this->apiResponse->status->setStatus(Status::STATUS_ERROR_PROCESO);
        }
    }
}