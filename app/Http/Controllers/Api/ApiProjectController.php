<?php namespace App\Http\Controllers\Api;

use App\Activation;
use App\Project;
use App\Report;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use PhpOffice\PhpPresentation\PhpPresentation;
use PhpOffice\PhpPresentation\IOFactory;
use PhpOffice\PhpPresentation\Style\Color;
use PhpOffice\PhpPresentation\Style\Alignment;
use PhpOffice\PhpPresentation\Style\Bullet;
use PhpOffice\PhpPresentation\Style\Border;
use PhpOffice\PhpPresentation\Shape\Chart\Type\AbstractTypeBar;
use PhpOffice\PhpPresentation\Shape\Chart\Series;
class ApiProjectController extends ApiController {

    public function __construct()
    {
        parent::__construct('App\Project');
    }

    public function anySelect($_code = null)
    {
        $user=\Auth::user()->load('roles');
            
        if($_code)
        {
            
            if($user->roles[0]->display_name=='Administrador')
            {
                $entries = Project::where('code', $_code)->with('producer', 'activations', 'indicators','client')->orderBy('status','asc')->orderBy('date_start','desc')->first();
                $entries->activations->load('images');
            }

            if($user->roles[0]->display_name=='Cliente')
            {
                $entries = Project::where('code', $_code)->with('producer', 'activations', 'indicators','client')->where('client_id',\Auth::user()->id)->orderBy('status','asc')->orderBy('date_start','desc')->first();
                $entries->activations->load('images');
            }

            if($user->roles[0]->display_name=='Productor')
            {
                $entries = Project::where('code', $_code)->with('producer', 'activations', 'indicators','client')->where('producer_id',\Auth::user()->id)->orderBy('status','asc')->orderBy('date_start','desc')->first();
                $entries->activations->load('images');
            }
        }else{
            
            if($user->roles[0]->display_name=='Administrador')
            {
                $entries = Project::orderBy('status','asc')->orderBy('date_start','desc')->get();
            }

            if($user->roles[0]->display_name=='Productor')
            {
                $entries = Project::orderBy('status','asc')->orderBy('date_start','desc')->where('producer_id',\Auth::user()->id)->get();
            }

            if($user->roles[0]->display_name=='Cliente')
            {
                $entries = Project::orderBy('status','asc')->orderBy('date_start','desc')->where('client_id',\Auth::user()->id)->get();
            }
        }

        $this->apiResponse->data = $entries;

        return response()->json($this->apiResponse);
    }

    public function anyExcel()
    {
        $project=Project::where('code','=', Input::get('code'))->get();
        $project->load('client');
        $project->load('activations');
        $project->load('producer');
        $project->load('resources');
        $project->load(['indicators'=>function($query){
            $query->orderBy('group','asc');
        }]);

        $data = array ();
        $count = 0;
        $credentials = null;
        $groups=[' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '];

        $merge_cell=array ();
        $group='';

        foreach ($project as $valproject) {

            $data_project = array ();
            $data_project[0] = array ('Código Campaña');
            $data_project[1] = array ('Nombre');
            $data_project[2] = array ('Fecha de inicio');
            $data_project[3] = array ('Fecha de fin');
            $data_project[4] = array ('Descripción');
            $data_project[5] = array ();
            $data_project[0]['datos'] = strval($valproject->code);
            $data_project[1]['datos'] = strval($valproject->name);
            //$data_project[3]['datos'] = strval($project->client);
            $data_project[2]['datos'] = strval($valproject->date_start);
            $data_project[3]['datos'] = strval($valproject->date_end);
            $data_project[4]['datos'] = strval($valproject->description);
            $data_project[5]['datos'] = '';

            foreach($valproject->activations as $valactivation)
            {
                $supervisor=DB::table('activation_user')->where('activation_id',$valactivation->id)->join('users','users.id','=','activation_user.user_id')->select('users.id as user_id','users.name','users.last_name')->get();

                $data[$count]['Fecha'] = $valactivation->date;
                $data[$count]['Cod Cliente SAP'] = $valactivation->code_sap;
                $data[$count]['Latitud'] = $valactivation->lat;
                $data[$count]['Longitud'] = $valactivation->lon;
                $data[$count]['Cliente'] = $valproject->client->name;
                $data[$count]['Razon Comercial'] = $valproject->client->company;
                $data[$count]['Cliente'] = $valproject->client->name;
                $data[$count]['Dirección'] = $valproject->client->address;
                $data[$count]['Distrito'] = $valproject->client->district;
                $data[$count]['Referencia'] = $valproject->client->reference;
                //$data[$count]['Supervisor'] = $supervisor[0]->name.' '.$supervisor[0]->last_name;
                $data[$count]['Sub-canal'] = $valactivation->sub_canal;
                $data[$count]['Hora de activación'] = 'De '.$valactivation->h_start.' a '.$valactivation->h_end ;
                foreach($valproject->indicators as $indicator)
                {

                    if($count==0)
                    {
                        if($group!=$indicator->group)
                        {
                            $group=$indicator->group;
                            if(count($merge_cell)==0)
                            {
                                array_push($merge_cell,[12,12]);
                            }
                            array_push($merge_cell,[$merge_cell[count($merge_cell)-1][1],$merge_cell[count($merge_cell)-1][1]]);
                            array_push($groups,$group);
                        }
                        else
                        {
                                array_push($groups,' ');
                                $merge_cell[count($merge_cell)-1][1]=$merge_cell[count($merge_cell)-1][1]+1;
                        }
                    }
                    if($indicator->type!='imagen')
                    {

                         $report=Report::where('activation_id',$valactivation->id)->where('indicator_id',$indicator->id)->first();
                        if(isset($report))
                            $data[$count][$indicator->name]=$report->value;
                        else
                            $data[$count][$indicator->name]='';
                    }
                  
                }
                $count++;
            }
            /*foreach($data as $key=>$valdata)
            {
                return $key;
                foreach($valproject->indicators as $indicator)
                {
                    $data[$key][$indicator->name]='';
                }
            }*/
        }

        if (count ($data) != 0) {
            $cell = count ($data[0]) + 1;
        } else {
            $cell = 1;
        }

        switch ($cell) {
            case 1:
                $cell = ':A';
                break;
            case 2:
                $cell = ':B';
                break;
            case 3:
                $cell = ':C';
                break;
            case 4:
                $cell = ':D';
                break;
            case 5:
                $cell = ':E';
                break;
            case 6:
                $cell = ':F';
                break;
            case 7:
                $cell = ':G';
                break;
            case 8:
                $cell = ':H';
                break;
            case 9:
                $cell = ':I';
                break;
            case 10:
                $cell = ':J';
                break;
            case 11:
                $cell = ':K';
                break;
            case 12:
                $cell = ':L';
                break;
            case 13:
                $cell = ':M';
                break;
            case 14:
                $cell = ':N';
                break;
            case 15:
                $cell = ':O';
                break;
            case 16:
                $cell = ':P';
                break;
            case 17:
                $cell = ':Q';
                break;
            case 18:
                $cell = ':R';
                break;
            case 19:
                $cell = ':S';
                break;
            case 20:
                $cell = ':T';
                break;
            case 21:
                $cell = ':U';
                break;
            case 22:
                $cell = ':V';
                break;
            case 23:
                $cell = ':Q';
                break;
            case 24:
                $cell = ':X';
                break;
            case 25:
                $cell = ':Y';
                break;
            case 25:
                $cell = ':Z';
                break;
        }


        Excel::create ('Reporte_cuantitativo_'.Input::get('code'), function ($excel) use ($data, $cell,$data_project,$groups,$merge_cell) {

            $excel->sheet ('Registro de activaciones', function ($sheet) use ($data, $cell,$data_project,$groups,$merge_cell) {
                //$sheet->fromArray ($data, true, 'B' . '2', true, true);
                if (count ($data) != 0) {
                    $num_border=count($data_project)+count($data)+3;
                    $a=count($data);
                    $cnt=0;
                    $sheet->row('8', $groups);
                    foreach($merge_cell as $c)
                    {
                        $letra = chr(65+$c[0]);
                        $letra2=chr(65+$c[1]);
                        //echo $letra .' '.$letra2;
                       // $sheet->fromArray ($groups, true, $letra . '8', true, true);


                        if($letra!=$letra2)
                        {
                            $sheet->mergeCells($letra.'8:'.$letra2.'8');
                        }

                       /* $sheet->fromArray ([], false, $letra . '3', false, false);
                        $cnt++;*/
                    }

                   $sheet->fromArray ($data_project, false, 'B' . '3', false, false);
                   $sheet->fromArray ($data, true, 'B' . '3', true, true);


                    $num_bold = 9;
                    $sheet->cells ('B' . ($num_bold) . $cell . $num_bold, function ($cells) {
                        $cells->setBackground ('#ff9835');
                        $cells->setFontColor ('#ffffff');
                    });
                    if(count($merge_cell)>0)
                    {
                        $sheet->cells (chr(65+$merge_cell[0][0]) . 8 . $cell . 8, function ($cells) {
                            $cells->setBackground ('#ff9835');
                            $cells->setFontColor ('#ffffff');
                            $cells->setFont (array (
                                'family' => 'Calibri',
                                'size' => '11',
                                'bold' => true,
                                $cells->setAlignment('center')
                            ));
                        });
                        $sheet->setBorder (chr(65+$merge_cell[0][0]) . 8 . $cell . 8, 'thin', false, '#C1C1C1');
                    }

                    $sheet->setBorder ('B' . $num_bold . $cell . $num_border, 'thin', false, '#C1C1C1');

                    $sheet->cells ('B' . $num_bold . $cell . $num_bold, function ($cells) {
                        $cells->setFont (array (
                            'family' => 'Calibri',
                            'size' => '11',
                            'bold' => true
                        ));
                    });
                }
            });
        })->export ('xls');
    }
    public function anyPpt()
    {
        $project=Project::where('code','=', Input::get('code'))->get();
        $fechainicio=$project[0]->date_start;
        $fechahoy=getdate();
        $mes=$fechahoy['mon'];
        $day=$fechahoy['mday'];
        if($fechahoy['mon']<10)
        {
            $mes='0'.$fechahoy['mon'];
        }
        if($fechahoy['mday']<10)
        {
            $day='0'.$fechahoy['mday'];
        }
        $fechahoy=$fechahoy['year'].'-'.$mes.'-'.$day;
        if($fechahoy>$project[0]->date_end)$fechahoy=$project[0]->date_end;


        // Create new PHPPowerPoint object
        $objPHPPowerPoint = new PhpPresentation();
        // Set properties

// Create slide
        $objPHPPowerPoint->removeSlideByIndex(0);
        for($i=$fechainicio;$i<=$fechahoy;$i = date("Y-m-d", strtotime($i ."+ 1 days"))){
            $weekNum = date("W") - date("W",strtotime(date("Y-m-01"))) + 1;
            $fecha = $i; //5 agosto de 2004 por ejemplo
            $fechats = strtotime($fecha); //a timestamp
            $d= date('d');
            switch (date('w', $fechats)){
                case 0: $fechats= "Domingo"; break;
                case 1: $fechats ="Lunes"; break;
                case 2: $fechats= "Martes"; break;
                case 3: $fechats= "Miercoles"; break;
                case 4: $fechats= "Jueves"; break;
                case 5: $fechats= "Viernes"; break;
                case 6: $fechats= "Sábado"; break;
            }
            $countactivation=Activation::where('date',$i)->where('project_id', $project[0]->id)->count();
            if($countactivation>0)
            {


                $currentSlide=$objPHPPowerPoint->createSlide();
// Create a shape (drawing)
                $shape = $currentSlide->createDrawingShape();
                $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
                $shape->setName('PHPPowerPoint logo')->setDescription('PHPPowerPoint logo')->setPath('img/logo.png')->setHeight(80)->setOffsetX(750)->setOffsetY(630);
                $shape = $currentSlide->createRichTextShape()->setHeight(300)->setWidth(700)->setOffsetX(50)->setOffsetY(470);
                $shape->createBreak();
                $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
                $textRun = $shape->createTextRun('INFORME CUALITATIVO');
                $textRun->getFont()->setBold(false)->setSize(35)->setColor(new Color('c00000'));
                $shape->createBreak();
                $textRun = $shape->createTextRun($project[0]->name);
                $textRun->getFont()->setBold(false)->setSize(45)->setColor(new Color('ffc000'));
                $shape->createBreak();
                $textRun = $shape->createTextRun($project[0]->description.' Semana '.$weekNum.' - '.$fechats.' '.$d);
                $textRun->getFont()->setBold(false)->setSize(35)->setColor(new Color('c00000'));
                $shape->createBreak();
                $canal=0;
                while($canal<2) {
                    if ($canal == 0) {$acton = Activation::with('images')->where('sub_canalv2', 'on')->where('project_id', $project[0]->id)->where('date',$i)->orderBy('area', 'DESC')->get();
                        $textcanal = 'ON';

                    } else {
                        $acton = Activation::with('images')->where('sub_canalv2', 'off')->where('project_id', $project[0]->id)->where('date',$i)->orderBy('area', 'DESC')->get();
                        $textcanal = 'OFF';
                    }
                    $canal++;
                    $currentSlide = $objPHPPowerPoint->createSlide();
                    $shape = $currentSlide->createDrawingShape();
                    $currentSlide->createLineShape(10, 630, 920, 630)->getBorder()->setColor(new Color('efeded'));
                    $shape->setName('PHPPowerPoint logo')->setDescription('PHPPowerPoint logo')->setPath('img/logo.png')->setHeight(80)->setOffsetX(750)->setOffsetY(630);
                    $shape = $currentSlide->createRichTextShape()->setHeight(300)->setWidth(600)->setOffsetX(50) ->setOffsetY(470);
                    $shape->createBreak();
                    $shape->createBreak();
                    $shape->createBreak();
                    $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
                    $textRun = $shape->createTextRun('Sub-Canal');
                    $textRun->getFont()->setBold(false)->setSize(35)->setColor(new Color('ffc000'));
                    $shape->createBreak();
                    $textRun = $shape->createTextRun($textcanal);
                    $textRun->getFont()->setBold(false)->setSize(45)->setColor(new Color('c00000'));
                    $shape->createBreak();
                    $count = 2;
                    $x = 10;
                    $y = 80;
                    $area_actual = '';

                    foreach ($acton as $act) {

                        $area_anterior = $act->area;
                        if ($count == 2 || $area_anterior != $area_actual) {
                            $area_actual = $act->area;
                            $count = 0;
                            $y = 80;
                            $currentSlide = $objPHPPowerPoint->createSlide();
                            $shape = $currentSlide->createDrawingShape();
                            $currentSlide->createLineShape(10, 630, 920, 630)->getBorder()->setColor(new Color('efeded'));
                            $shape->setName('PHPPowerPoint logo')->setDescription('PHPPowerPoint logo')->setPath('img/logo.png')->setHeight(80)->setOffsetX(750)->setOffsetY(630);
                            $shape = $currentSlide->createRichTextShape()->setHeight(50)->setWidth(150)->setOffsetX(800)->setOffsetY(10);
                            $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                            $shape->getBorder()->setColor(new Color('c00000'))->setLineWidth(10)->setLineStyle(Border::LINE_SINGLE);
                            $textRun = $shape->createTextRun($area_actual);
                            $textRun->getFont()->setBold(true)->setSize(25)->setColor(new Color('000000'));
                        }
                        $shape = $currentSlide->createRichTextShape()->setHeight(300)->setWidth(470)->setOffsetX(500)->setOffsetY($y);
                        $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
                        $textRun = $shape->createTextRun($act->agent_comercial);
                        $textRun->getFont()->setBold(true)->setSize(20)->setColor(new Color('000000'));
                        $shape->createBreak();
                        $shape->createBreak();
                        $textRun = $shape->createTextRun('Código	:');
                        $textRun->getFont()->setBold(false)->setSize(18)->setColor(new Color('000000'));
                        $textRun = $shape->createTextRun($act->code_local);
                        $textRun->getFont()->setBold(false)->setSize(18)->setColor(new Color('000000'));
                        $shape->createBreak();
                        $textRun = $shape->createTextRun('Distrito	:');
                        $textRun->getFont()->setBold(false)->setSize(18)->setColor(new Color('000000'));
                        $textRun = $shape->createTextRun($act->district);
                        $textRun->getFont()->setBold(false)->setSize(18)->setColor(new Color('000000'));
                        $shape->createBreak();
                        $textRun = $shape->createTextRun('Observaciones:');
                        $textRun->getFont()->setBold(false)->setSize(18)->setColor(new Color('000000'));
                        $textRun = $shape->createTextRun($act->observation);
                        $textRun->getFont()->setBold(false)->setSize(18)->setColor(new Color('000000'));
                        $shape->createBreak();
                        if (count($act['images']) > 1) {
                            foreach ($act['images'] as $img) {
                                $shape2 = $currentSlide->createDrawingShape();
                                $shape2->setName('PHPPowerPoint logo')->setDescription('PHPPowerPoint logo')->setPath($img->path)->setHeight(150)->setOffsetX($x)->setOffsetY($y);
                                $x = $x + 250;
                            }
                        } else {
                            $path='img/report_default.png';
                            if(count($act['images']) ==1){$path=$act['images'][0]->path;}
                            $shape2 = $currentSlide->createDrawingShape();
                            $shape2->setName('PHPPowerPoint logo')->setDescription('PHPPowerPoint logo')->setPath($path)->setHeight(200)->setOffsetX(120)->setOffsetY($y);
                        }
                        $count++;
                        $y = $y + 300;
                        $x = 10;
                    }
                }
            }
        }
        ///
        $currentSlide=$objPHPPowerPoint->createSlide();
        $shape = $currentSlide->createDrawingShape();
        $currentSlide->createLineShape(10,70,920,70)->getBorder()->setColor(new Color('efeded'));
        $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
        $shape->setName('PHPPowerPoint logo')->setDescription('PHPPowerPoint logo')->setPath('img/logo.png')->setHeight(150)->setOffsetX(250)->setOffsetY(250);
        // Save file
        $oWriterPPTX = IOFactory::createWriter($objPHPPowerPoint, 'PowerPoint2007');
        $oWriterPPTX->save( "reporte.pptx");
        $oWriterODP = IOFactory::createWriter($objPHPPowerPoint, 'ODPresentation');
        $oWriterODP->save("reporte.odp");
        $filename = str_replace('.php', '.pptx', "reporte.pptx");
        $newname = "reporte-" . date('Y-m-d-H-i-s') . ".pptx";
        return response()->download($filename);
    }

    public function anyPptdetails()
    {
        $project=Project::where('code','=', Input::get('code'))->get();

        //$project = Project::get();
        $project->load('client');
        $project->load('activations');
        $project->load('producer');
        $project->load('resources');
        $project->load('indicators');
        // Create new PHPPowerPoint object
        $objPHPPowerPoint = new PhpPresentation();
// Set properties

        $objPHPPowerPoint->getProperties()->setCreator('PHPOffice')
            ->setLastModifiedBy('PHPPowerPoint Team')
            ->setTitle('Pimienta')
            ->setSubject('report 01 Subject')
            ->setDescription('report 01 Description')
            ->setKeywords('office 2007 openxml libreoffice odt php')
            ->setCategory('Sample Category');
// Create slide

        $objPHPPowerPoint->removeSlideByIndex(0);
        $currentSlide=$objPHPPowerPoint->createSlide();

        //       return $project;
// Create a shape (drawing)

        $shape = $currentSlide->createDrawingShape();
        $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
        $shape->setName('PHPPowerPoint logo')
            ->setDescription('PHPPowerPoint logo')
            ->setPath('img/logo.png')
            ->setHeight(80)
            ->setOffsetX(750)
            ->setOffsetY(630);
        $shape = $currentSlide->createRichTextShape()
            ->setHeight(300)
            ->setWidth(600)
            ->setOffsetX(50)
            ->setOffsetY(470);
        $shape->createBreak();
        $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $textRun = $shape->createTextRun($project[0]->code);
        $textRun->getFont()->setBold(false)->setSize(35)->setColor(new Color('767171'));
        $shape->createBreak();
        $textRun = $shape->createTextRun($project[0]->name);

        $textRun->getFont()->setBold(false)->setSize(45)->setColor(new Color('4472c4'));
        $shape->createBreak();
        $textRun = $shape->createTextRun($project[0]->description);
        $textRun->getFont()->setBold(false)->setSize(35)->setColor(new Color('767171'));
        $shape->createBreak();
        $currentSlide=$objPHPPowerPoint->createSlide();
        $shape = $currentSlide->createDrawingShape();
        $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
        $shape->setName('PHPPowerPoint logo')
            ->setDescription('PHPPowerPoint logo')
            ->setPath('img/logo.png')
            ->setHeight(80)
            ->setOffsetX(750)
            ->setOffsetY(630);
        $shape = $currentSlide->createRichTextShape()
            ->setHeight(300)
            ->setWidth(600)
            ->setOffsetX(50)
            ->setOffsetY(370);
        $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $textRun=$shape->createTextRun('Activos/ ');
        $textRun->getFont()->setBold(false)->setSize(60)->setColor(new Color('4472c4'));
        $currentSlide=$objPHPPowerPoint->createSlide();
        $shape = $currentSlide->createDrawingShape();
        $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
        $shape->setName('PHPPowerPoint logo')
            ->setDescription('PHPPowerPoint logo')
            ->setPath('img/logo.png')
            ->setHeight(80)
            ->setOffsetX(750)
            ->setOffsetY(630);
        $shape = $currentSlide->createRichTextShape()
            ->setHeight(300)
            ->setWidth(600)
            ->setOffsetX(50)
            ->setOffsetY(200);
        $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $textRun=$shape->createTextRun('ACTIVOS IMPLEMENTADOS: ');
        $textRun->getFont()->setBold(false)->setSize(18)->setColor(new Color('4472c4'));
        $shape->createBreak();
        $count=1;
        $x=350;
        $y=40;
        foreach($project[0]['resources'] as $desc)
        {
            $textRun = $shape->createTextRun($count.'.  ');
            $textRun->getFont()->setBold(true)->setSize(18)->setColor(new Color('767171'));
            $textRun = $shape->createTextRun( $desc->description);
            $textRun->getFont()->setBold(false)->setSize(18)->setColor(new Color('767171'));
            $shape->createBreak();
            $shape2 = $currentSlide->createDrawingShape();
            $desc->load('images');

            if($count%4==0)
            {
                $y=$y+150;
                $x=350;
                $shape2->setName('PHPPowerPoint resource')
                    ->setDescription('PHPPowerPoint resource')
                    ->setPath($desc['images'][0]->path)
                    // ->setPath('img/20847-tiger-in-the-shadows-1680x1050-digital-art-wallpaper.jpg')
                    ->setHeight(120)
                    ->setOffsetX($x)
                    ->setOffsetY($y);
            }
            else
            {
                $shape2->setName('PHPPowerPoint resource')
                    ->setDescription('PHPPowerPoint resource')
                    ->setPath($desc['images'][0]->path)
                    //->setPath('img/20847-tiger-in-the-shadows-1680x1050-digital-art-wallpaper.jpg')
                    ->setHeight(120)
                    ->setOffsetX($x)
                    ->setOffsetY($y);
                $x=$x+200;
            }
            $count++;

        }
        $currentSlide=$objPHPPowerPoint->createSlide();
        $shape = $currentSlide->createDrawingShape();
        $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
        $shape->setName('PHPPowerPoint logo')
            ->setDescription('PHPPowerPoint logo')
            ->setPath('img/logo.png')
            ->setHeight(80)
            ->setOffsetX(750)
            ->setOffsetY(630);
        $shape = $currentSlide->createRichTextShape()
            ->setHeight(300)
            ->setWidth(600)
            ->setOffsetX(50)
            ->setOffsetY(370);
        $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $textRun=$shape->createTextRun('Activación/ ');
        $textRun->getFont()->setBold(false)->setSize(60)->setColor(new Color('4472c4'));
        ////for activaciones////

        $currentSlide=$objPHPPowerPoint->createSlide();
        $shape = $currentSlide->createDrawingShape();
        $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
        $shape->setName('PHPPowerPoint logo')
            ->setDescription('PHPPowerPoint logo')
            ->setPath('img/logo.png')
            ->setHeight(80)
            ->setOffsetX(750)
            ->setOffsetY(630);
        $shape = $currentSlide->createRichTextShape()
            ->setHeight(300)
            ->setWidth(420)
            ->setOffsetX(50)
            ->setOffsetY(90);
        $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $textRun=$shape->createTextRun('FROSTER ICE: ');
        $textRun->getFont()->setBold(false)->setSize(16)->setColor(new Color('4472c4'));
        $shape->createBreak();
        $textRun=$shape->createTextRun('Desde la segunda hora de la activación, los personajes congelados usaron una mochila cooler que contenía Backus Ice de 630 ml. Así salieron acompañados de dos anfitrionas recorriendo toda la discoteca ofreciendo la
venta de Backus Ice y la promoción vigente(1 cerveza de 630 x s/.10). Con la ayuda de los 02 teams de mochileros-anfitrionas, se pudo vender casi 4 cajas de cerveza; no pudiendo vender
más debido a que el local sólo contaba con 15 cajas en stock y en esta fecha, barrimos dicho stock por completo.
Nuestro producto tenía el mismo precio que las cervezas Cristal y Pilsen, además siguiendo la mecánica del local, la cerveza debíamos de servirla en vasos. Sin embargo, los únicos que encontramos en la discoteca fueron de cerveza');
        $textRun->getFont()->setBold(false)->setSize(16)->setColor(new Color('767171'));
        $shape->createBreak();
        $shape2 = $currentSlide->createDrawingShape();

        $shape2->setName('PHPPowerPoint resource')
            ->setDescription('PHPPowerPoint resource')
            ->setPath('img/20847-tiger-in-the-shadows-1680x1050-digital-art-wallpaper.jpg')
            ->setHeight(200)
            ->setOffsetX(600)
            ->setOffsetY(100);
        $shape2 = $currentSlide->createDrawingShape();

        $shape2->setName('PHPPowerPoint resource')
            ->setDescription('PHPPowerPoint resource')
            ->setPath('img/20847-tiger-in-the-shadows-1680x1050-digital-art-wallpaper.jpg')
            ->setHeight(200)
            ->setOffsetX(500)
            ->setOffsetY(300);
        //2
        $currentSlide=$objPHPPowerPoint->createSlide();
        $shape = $currentSlide->createDrawingShape();
        $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
        $shape->setName('PHPPowerPoint logo')
            ->setDescription('PHPPowerPoint logo')
            ->setPath('img/logo.png')
            ->setHeight(80)
            ->setOffsetX(750)
            ->setOffsetY(630);
        $shape = $currentSlide->createRichTextShape()
            ->setHeight(300)
            ->setWidth(900)
            ->setOffsetX(30)
            ->setOffsetY(30);
        $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $textRun=$shape->createTextRun('MOCHILEROS:');
        $textRun->getFont()->setBold(false)->setSize(16)->setColor(new Color('4472c4'));
        $shape->createBreak();
        $textRun=$shape->createTextRun('A las 05:30 pm se activó el momento Ice escuchándose el track de Ice Ice Baby; en este momento 02 breakdancers, acompañados de 02 bailarinas, realizaron una coreografía, todos vistiendo el uniforme de Backus ICE, realizaron una
coreografía en la cual se utilizaron pistolas de CO2, captando la atención del público durante todo el tiempo que se presentaron los bailarines. El público se emocionó con los pasos de baile de los breakdancers y después quedaron prendados del baile de las 02 bailarinas.');
        $textRun->getFont()->setBold(false)->setSize(16)->setColor(new Color('767171'));
        $shape->createBreak();
        $shape2 = $currentSlide->createDrawingShape();

        $shape2->setName('PHPPowerPoint resource')
            ->setDescription('PHPPowerPoint resource')
            ->setPath('img/20847-tiger-in-the-shadows-1680x1050-digital-art-wallpaper.jpg')
            ->setHeight(200)
            ->setOffsetX(100)
            ->setOffsetY(400);
        $shape2 = $currentSlide->createDrawingShape();

        $shape2->setName('PHPPowerPoint resource')
            ->setDescription('PHPPowerPoint resource')
            ->setPath('img/20847-tiger-in-the-shadows-1680x1050-digital-art-wallpaper.jpg')
            ->setHeight(200)
            ->setOffsetX(500)
            ->setOffsetY(400);
        //3
        $currentSlide=$objPHPPowerPoint->createSlide();
        $shape = $currentSlide->createDrawingShape();
        $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
        $shape->setName('PHPPowerPoint logo')
            ->setDescription('PHPPowerPoint logo')
            ->setPath('img/logo.png')
            ->setHeight(80)
            ->setOffsetX(750)
            ->setOffsetY(630);
        $shape = $currentSlide->createRichTextShape()
            ->setHeight(300)
            ->setWidth(420)
            ->setOffsetX(50)
            ->setOffsetY(90);
        $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $textRun=$shape->createTextRun('PREMIACION: ');
        $textRun->getFont()->setBold(false)->setSize(16)->setColor(new Color('4472c4'));
        $shape->createBreak();
        $textRun=$shape->createTextRun('Con la presencia del administrador realizamos la premiación a la mesera que vendió más Backus Ice. Para poder realizar el conteo de las ventas se les pidió a las meseras que juntaran las chapas de las botellas vendidas.
Datos:
Nombre: Rosa Villar Balboa.
DNI: 70903778
Cel: 952960054
Chapas: 70');
        $textRun->getFont()->setBold(false)->setSize(16)->setColor(new Color('767171'));
        $shape->createBreak();
        $shape2 = $currentSlide->createDrawingShape();

        $shape2->setName('PHPPowerPoint resource')
            ->setDescription('PHPPowerPoint resource')
            ->setPath('img/20847-tiger-in-the-shadows-1680x1050-digital-art-wallpaper.jpg')
            ->setHeight(200)
            ->setOffsetX(600)
            ->setOffsetY(100);

        ////end activacion
        ////hallazgos
        $currentSlide=$objPHPPowerPoint->createSlide();
        $shape = $currentSlide->createDrawingShape();
        $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
        $shape->setName('PHPPowerPoint logo')
            ->setDescription('PHPPowerPoint logo')
            ->setPath('img/logo.png')
            ->setHeight(80)
            ->setOffsetX(750)
            ->setOffsetY(630);
        $shape = $currentSlide->createRichTextShape()
            ->setHeight(300)
            ->setWidth(600)
            ->setOffsetX(50)
            ->setOffsetY(370);
        $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $textRun=$shape->createTextRun('Hallazgos/ ');
        $textRun->getFont()->setBold(false)->setSize(60)->setColor(new Color('4472c4'));
        $currentSlide=$objPHPPowerPoint->createSlide();
        $shape = $currentSlide->createDrawingShape();
        $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
        $shape->setName('PHPPowerPoint logo')
            ->setDescription('PHPPowerPoint logo')
            ->setPath('img/logo.png')
            ->setHeight(80)
            ->setOffsetX(750)
            ->setOffsetY(630);
        $shape = $currentSlide->createRichTextShape()
            ->setHeight(300)
            ->setWidth(420)
            ->setOffsetX(50)
            ->setOffsetY(90);
        $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $textRun=$shape->createTextRun('ACTIVACION LUCKY STRIKE ');
        $textRun->getFont()->setBold(false)->setSize(16)->setColor(new Color('4472c4'));
        $shape->createBreak();
        $textRun=$shape->createTextRun('En esta fecha, Lucky Strike también realizó una activación en donde un equipo de promotor y anfitrionas repartían licores (vodka y pisco) provistos de una mochila dispensador.');
        $textRun->getFont()->setBold(false)->setSize(16)->setColor(new Color('767171'));
        $shape->createBreak();
        $shape2 = $currentSlide->createDrawingShape();

        $shape2->setName('PHPPowerPoint resource')
            ->setDescription('PHPPowerPoint resource')
            ->setPath('img/20847-tiger-in-the-shadows-1680x1050-digital-art-wallpaper.jpg')
            ->setHeight(200)
            ->setOffsetX(600)
            ->setOffsetY(100);
        $shape2 = $currentSlide->createDrawingShape();
        $shape2->setName('PHPPowerPoint resource')
            ->setDescription('PHPPowerPoint resource')
            ->setPath('img/20847-tiger-in-the-shadows-1680x1050-digital-art-wallpaper.jpg')
            ->setHeight(200)
            ->setOffsetX(100)
            ->setOffsetY(400);
        $shape2 = $currentSlide->createDrawingShape();
        $shape2->setName('PHPPowerPoint resource')
            ->setDescription('PHPPowerPoint resource')
            ->setPath('img/20847-tiger-in-the-shadows-1680x1050-digital-art-wallpaper.jpg')
            ->setHeight(200)
            ->setOffsetX(500)
            ->setOffsetY(400);
        //end hallazgos
        //// resumen
        $currentSlide=$objPHPPowerPoint->createSlide();
        $shape = $currentSlide->createDrawingShape();
        $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
        $shape->setName('PHPPowerPoint logo')
            ->setDescription('PHPPowerPoint logo')
            ->setPath('img/logo.png')
            ->setHeight(80)
            ->setOffsetX(750)
            ->setOffsetY(630);
        $shape = $currentSlide->createRichTextShape()
            ->setHeight(300)
            ->setWidth(900)
            ->setOffsetX(50)
            ->setOffsetY(370);
        $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $textRun=$shape->createTextRun('Resumen/ ');
        $textRun->getFont()->setBold(false)->setSize(60)->setColor(new Color('4472c4'));
        //end resumen
        //fotos
        $currentSlide=$objPHPPowerPoint->createSlide();
        $shape = $currentSlide->createDrawingShape();
        $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
        $shape->setName('PHPPowerPoint logo')
            ->setDescription('PHPPowerPoint logo')
            ->setPath('img/logo.png')
            ->setHeight(80)
            ->setOffsetX(750)
            ->setOffsetY(630);
        $shape = $currentSlide->createRichTextShape()
            ->setHeight(300)
            ->setWidth(900)
            ->setOffsetX(50)
            ->setOffsetY(370);
        $shape->getActiveParagraph()->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $textRun=$shape->createTextRun('Fotos de la activación/ ');
        $textRun->getFont()->setBold(false)->setSize(60)->setColor(new Color('4472c4'));
        $currentSlide=$objPHPPowerPoint->createSlide();
        $shape = $currentSlide->createDrawingShape();
        $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
        $shape->setName('PHPPowerPoint logo')
            ->setDescription('PHPPowerPoint logo')
            ->setPath('img/logo.png')
            ->setHeight(80)
            ->setOffsetX(750)
            ->setOffsetY(630);
        $shape = $currentSlide->createDrawingShape();
        $shape->setName('PHPPowerPoint resource')
            ->setDescription('PHPPowerPoint resource')
            ->setPath('img/20847-tiger-in-the-shadows-1680x1050-digital-art-wallpaper.jpg')
            ->setHeight(400)
            ->setOffsetX(150)
            ->setOffsetY(150);
        ///
        $currentSlide=$objPHPPowerPoint->createSlide();
        $shape = $currentSlide->createDrawingShape();
        $currentSlide->createLineShape(10,70,920,70)->getBorder()->setColor(new Color('efeded'));
        $currentSlide->createLineShape(10,630,920,630)->getBorder()->setColor(new Color('efeded'));
        $shape->setName('PHPPowerPoint logo')
            ->setDescription('PHPPowerPoint logo')
            ->setPath('img/logo.png')
            ->setHeight(150)
            ->setOffsetX(250)
            ->setOffsetY(250);
// Save file


        $oWriterPPTX = IOFactory::createWriter($objPHPPowerPoint, 'PowerPoint2007');
        $oWriterPPTX->save( __DIR__ ."reporte.pptx");
        $oWriterODP = IOFactory::createWriter($objPHPPowerPoint, 'ODPresentation');
        $oWriterODP->save(__DIR__ ."reporte.odp");
        $filename = str_replace('.php', '.pptx', "reporte.pptx");
        $newname = "reporte-" . date('Y-m-d-H-i-s') . ".pptx";
        return response()->download($filename);


    }
    public function anySaveproyect()
    {

        $data = Input::all();
        $project=new Project();
        $code='sdfkjsdkfnskddfjsdfjsdfjsdfsjkdfhkjsdf';
        if(isset($data['date_start']) )
        {
            $date=new \DateTime($data['date_start']);
            $ds=date_format($date,'Y-m-d');
            $data['date_start']=$ds;
        }
        if(isset($data['date_end']) )
        {
            $date=new \DateTime($data['date_end']);
            $ds=date_format($date,'Y-m-d');
            $data['date_end']=$ds;
        }
        if(isset($data['id']))
        {
            $project= Project::find($data['id']);
            $code=$project->code;
        }
            if($code==$data['code'])
            {
                $validator=$project->isValid2($data);
            }
            else{
                $validator=$project->isValid($data);
            }


            if($validator->passes())
            {
                    $project->fill($data);
                    $project->save();
                if(!isset($data['id']))
                {
                    DB::table('indicators')->insert([
                        ['project_id' => $project->id, 'name' => 'Activación','details'=>'¿Se realizó la Activación?','group'=>'Activación','type'=>'booleano'],
                        ['project_id' => $project->id, 'name' => 'Detalle Activación','details'=>'Descripción de la Activacion','group'=>'Activación','type'=>'libre'],
                        ['project_id' => $project->id, 'name' => 'Hora Llegada','details'=>'Hora de Llegada al POS','group'=>'Activación','type'=>'libre'],
                        ['project_id' => $project->id, 'name' => 'Hora de Salida','details'=>'Hora de Salida del POS','group'=>'Activación','type'=>'libre'],
                        ['project_id' => $project->id, 'name' => 'Observación Detallista','details'=>'Descripción del trabajo del equipo por el Detallista','group'=>'Activación','type'=>'libre'],
                        ['project_id' => $project->id, 'name' => 'Nombre Contacto en POS','details'=>'Nombre de Contacto en el POS','group'=>'Activación','type'=>'libre'],
                        ['project_id' => $project->id, 'name' => 'Teléfono Contacto en POS','details'=>'Teléfono de Contacto en POS','group'=>'Activación','type'=>'libre'],
                        ['project_id' => $project->id, 'name' => 'Calificación de Team','details'=>'Calificación dada por el equipo de activación por el Contacto en el POS','group'=>'Activación','type'=>'cualitativo'],
                        ['project_id' => $project->id, 'name' => 'Referencia del POS','details'=>'Referencia de ubicación de POS. No llenar si Referencia anterior funcionó.','group'=>'Activación','type'=>'libre'],
                    ]);
                }

            }
            else{
                $this->apiResponse->status->code="220";
                $this->apiResponse->status->description= "error en parametros";
                $this->apiResponse->status->details= $validator->messages();
            }
        return response()->json($this->apiResponse);
    }
}