<?php namespace App\Http\Controllers\Api;



use App\Activation;
use App\Http\Middleware\Status;
use App\Team;
use App\User;
use App\Project;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Monolog\Handler\ElasticSearchHandlerTest;
use Validator;


class ApiTeamController extends ApiController
{
    public function __construct ()
    {
        parent::__construct ('App\Team');
    }

    public function anyEdit()
    {
        if(Input::has('id'))
        {
            $data=Input::all();
            $team=Team::find(Input::get('id'));
            if(isset($team))
            {
                $team->number=$data['number'];
                $team->rol=$data['rol'];
                $team->save();

            }
        }
        else
            $this->apiResponse->setData(Status::STATUS_ERROR_PARAMETROS);

        return response()->json($this->apiResponse);
    }

    public function anySaveTeam()
    {
        $data=Input::all();
        $team=new Team();
        if($team->isValid($data))
        {
            $project=Project::where('code',$data['project_code'])->first();

            if(isset($project))
            {
                $team->project_id=$project->id;
                $team->user_id=$data['id'];
                $team->number=$data['number'];
                $team->rol=$data['rol'];
                $team->save();
            }
        }

        return response()->json($this->apiResponse);
    }

    public function anyList()
    {
        $project=Project::where('code',Input::get('project_code'))->first();
        if(isset($project))
        {
            $team=Team::where('project_id',$project->id)->get();
            $group=Team::where('project_id',$project->id)->distinct('number')->orderBy('number')->get(array('number'));
            $max_group=Team::where('project_id',$project->id)->max('number')+1;

            $number_group=array();
            if(count($group)!=0)
            {
                foreach($group as $valgroup)
                {
                    array_push($number_group,$valgroup->number);
                }
                array_push($number_group,$max_group);
            }
            else{
                array_push($number_group,'1');
            }
            $team->load('user');
            $team->load('project');
            $this->apiResponse->data=array('team'=>$team,'number_group'=>$number_group);


        }

        return response()->json($this->apiResponse);
    }

    public function anyGroup()
    {
        $group=array();
        $team=null;
        $supervisors=null;
        $project=Project::where('code',Input::get('project_code'))->first();

        if(isset($project))$team=Team::with('user')->where('project_id',$project->id)->orderBy('number','asc')->get();

        if(count($team)!='0' && isset($team))
        {
            foreach($team as $valteam)
            {
                $id =$valteam['number'];
                if (isset($result[$id])) {
                    $result[$id]['collaborator'][] = $valteam;
                } else {
                    $result[$id]['collaborator'] = array($valteam);
                }
                $team_id=Team::where('number',$valteam['number'])->where('project_id',$project->id)->lists('id');

                if(count($team_id)) {
                    $activation_id=DB::table('team_activation')->whereIn('team_id',$team_id)->lists('activation_id');
                    if(count($activation_id)!=0) {
                        $activation = Activation::find ($activation_id);
                        $result[$id]['activations'] = $activation;
                    }
                    else
                        $result[$id]['activations']=null;
                }

                $supervisor_id=DB::table('activation_user')->whereIn('activation_id',$activation_id)->lists('user_id');
                if(count($supervisor_id)!='0')$supervisor=User::find($supervisor_id);

                if(isset($supervisor)){
                    foreach($supervisor as $valsupervisor)
                    {
                        if($supervisors==null)
                        $supervisors=$valsupervisor->name.' '.$valsupervisor->lastname;
                        else
                            $supervisors=$supervisors.','.$valsupervisor->name.' '.$valsupervisor->lastname;
                    }
                }
                $result[$id]['supervisor']=$supervisors;

                $supervisors=null;
            }
            $this->apiResponse->data=$result;
        }
        return response()->json($this->apiResponse);
    }
    public function anyDeletecollaborator()
    {

        if(Input::has('id'))
        {
            $id=Input::get('id');
            $team=Team::find($id);
            $team->activations()->detach();
            $team->delete();
        }

    }
    public function anyDeletesupervisor()
    {

        if(Input::has('id'))
        {
            $id=Input::get('id');
            $user=User::find($id);
            $project=Project::where('code',Input::get('code'))->first();
            $activaciones=Activation::where('project_id',$project->id)->get();
            foreach($activaciones as $act)
            {
                DB::table('activation_user')->where('activation_id', '=', $act->id)->delete();
            }
            $user->projects()->detach($project->id);


        }
    }
}