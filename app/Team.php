<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Team extends Model
{
    protected $table='teams';
    protected $fillable=['number','description','user_id','activation_id'];

    public function project()
    {
        return $this->hasOne('App\Project','id','project_id');
    }

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    function isValid($input)
    {
        $rules = array('user_id' => 'required','number' => 'required|numeric','rol' => 'required');
        $validator = Validator::make ($input, $rules);
        return $validator;
    }

    public function activations()
    {
        return $this->belongsToMany('App\Activation','team_activation')
            ->withPivot('team_id','activation_id');
    }
}