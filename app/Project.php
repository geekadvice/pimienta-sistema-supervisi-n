<?php namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class Project extends Model
{

	protected $fillable = ['producer_id', 'client_id', 'name', 'code', 'description', 'date_start', 'date_end', 'status'];

	function isValid($input)
	{
		$rules = array('Nombre' => 'required',
            'code' => 'required|unique:projects',
            'Productor' => 'required',
            'Cliente'=> 'required',
            'Fecha_inicio'=> 'date_format:Y-m-d',
            'Fecha_fin' => 'date_format:Y-m-d|after:Fecha_inicio');
        $data = array ();
        foreach ($input as $key => $valinput) {
            if ($key == 'name')
                $data['Nombre'] = $valinput;

            if ($key == 'code')
                $data['code'] = $valinput;

            if ($key == 'producer_id')
                $data['Productor'] = $valinput;

            if ($key == 'client_id')
                $data['Cliente'] = $valinput;

            if ($key == 'date_start')
                $data['Fecha_inicio'] = $valinput;

            if ($key == 'date_end')
                $data['Fecha_fin'] = $valinput;

        }
        $validator = Validator::make($data, $rules);
        return $validator;
	}
    function isValid2($input)
    {
        $rules = array('Nombre' => 'required',
            'code' => 'required',
            'Productor' => 'required',
            'Cliente'=> 'required',
            'Fecha_inicio'=> 'date_format:Y-m-d',
            'Fecha_fin' => 'date_format:Y-m-d|after:Fecha_inicio');
        $data = array ();

        foreach ($input as $key => $valinput) {
            if ($key == 'name')
                $data['Nombre'] = $valinput;

            if ($key == 'code')
                $data['code'] = $valinput;

            if ($key == 'producer_id')
                $data['Productor'] = $valinput;

            if ($key == 'client_id')
                $data['Cliente'] = $valinput;

            if ($key == 'date_start')
                $data['Fecha_inicio'] = $valinput;

            if ($key == 'date_end')
                $data['Fecha_fin'] = $valinput;

        }
        $validator = Validator::make($data, $rules);
        return $validator;
    }
	public function client()
	{
		return $this->belongsTo('App\User', 'client_id');
	}

	public function producer()
	{
		return $this->belongsTo('App\User', 'producer_id');
	}
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }
	public function activations()
	{
		return $this->hasMany('App\Activation');
	}

	public function indicators()
	{
		return $this->hasMany('App\Indicator')->orderBy('group','asc');
	}

	public function resources()
	{
		return $this->hasMany('App\Resource');
	}

	public function team()
	{
		return $this->hasManyThrough('App\User', 'App\Activation');
	}
}
