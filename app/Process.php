<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Process extends Model
{
    protected $table='processes';
    protected $fillable = ['name', 'description','type'];

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }
}