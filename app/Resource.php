<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Resource extends Model
{

	protected $fillable = ['quantity', 'description', 'project_id'];

	function isValid($input)
	{
		$rules = array('name' => 'required', 'project_id' => 'required');
		$validator = Validator::make ($input, $rules);
		return $validator;
	}

	public function project()
	{
		return $this->belongsTo('App\Project');
	}
    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }
}
