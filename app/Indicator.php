<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Indicator extends Model
{

	protected $fillable = ['name', 'type', 'group', 'details', 'project_id'];

	function isValid($input)
	{
		$rules = array('name' => 'required', 'project_id' => 'required');
		$validator = Validator::make ($input, $rules);
		return $validator;
	}

	public function report()
	{
		return $this->hasMany('App\Report');
	}

	public function project()
	{
		return $this->belongsTo('App\Project');
	}

}
