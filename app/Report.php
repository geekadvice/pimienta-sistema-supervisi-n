<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Report extends Model
{

	protected $fillable = ['observation', 'value', 'user_id', 'indicator_id', 'activation_id'];

	function isValid($input)
	{
		$rules = array('value' => 'required', 'user_id' => 'required');
		$validator = Validator::make ($input, $rules);
		return $validator;
	}

	public function indicator()
	{
		return $this->belongsTo('App\Indicator')->orderBy('group','asc');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function activation()
	{
		return $this->belongsTo('App\Activation');
	}
    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

}
