<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Facades\Validator;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {
//class User extends Eloquent{

	use EntrustUserTrait;
	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password','address', 'district', 'reference','dni','last_name','company'];
    function isValid($input)
    {
        $rules = array('name' => 'required', 'email' => 'required|email','dni'=>'required','last_name'=> 'required');
        $validator = Validator::make ($input, $rules);
        return $validator;
    }
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function roles()
	{
		return $this->belongsToMany('App\Role','role_user');
	}
    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }
    public function projects()
    {
        return $this->belongsToMany('App\Project')->withTimestamps();
    }
    public function team()
    {
        return $this->belongsTo('App\Team','id','user_id');
    }
}
