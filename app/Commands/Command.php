<?php namespace App\Commands;
use App\User;
 class Command {

	//
    public function disabled()
    {

        $user=User::find(\Auth::id());
        if($user->disabled==1)
        {
            \Auth::logout();
            unset($_COOKIE['Laravel_session']);
            setcookie('Laravel_session', null, -1, '/');

            return 1;
        }
        return 0;
    }

}
