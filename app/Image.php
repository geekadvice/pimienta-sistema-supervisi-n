<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Image extends Model
{
    protected $table='images';
    protected $fillable = ['path', 'description'];

    public function imageable()
    {
        return $this->morphTo();
    }
    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }
}