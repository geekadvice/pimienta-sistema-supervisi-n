'use strict';

/* Services */

var APIResources = function($resource)
{
	return {
		routes:
		{
        loadExcel:appConfig.api + 'activations/loadexcel',
        saveProject:appConfig.api + 'projects/saveproyect',
        deleteProject:appConfig.api + 'projects/delete',
        saveIndicator:appConfig.api + 'indicators/save',
        deleteIndicator:appConfig.api + 'indicators/deleteindicator',
        saveActivationImage:appConfig.api + 'activations/image',
        saveUserImage:appConfig.api + 'users/image',
        saveActivation:appConfig.api + 'activations/save-observation',
        saveCollaborator:appConfig.api + 'teams/save-team',
        editCollaborator:appConfig.api + 'teams/edit',
        listCollaborator:appConfig.api + 'teams/list',
        groupCollaborator:appConfig.api + 'teams/group',
        statusactivation:appConfig.api + 'activations/statuschange',
        deleteCollaborator:appConfig.api + 'teams/deletecollaborator',
        deleteSupervisor:appConfig.api + 'teams/deletesupervisor',
        addactivationssupervisor:appConfig.api + 'activations/assignactivations',
        groupAssignation:appConfig.api+'activations/group-assignation',
        deleteImage:appConfig.api + 'images/delete',
        images:appConfig.api + 'images/list',
        listactivationgroup:appConfig.api + 'activations/groups',
        findActivation:appConfig.api + 'activations/find',
        saveIndicatorImage:appConfig.api + 'indicators/image',
        saveUsercamp:appConfig.api + 'users/adduserproyect',
        currentUser:appConfig.api + 'users/current',
        statusUser:appConfig.api + 'users/status',
        rolUser:appConfig.api + 'users/filterrole',
            editProfile:appConfig.api + 'users/updateuser'
		},
		users : $resource(appConfig.api + '/users'),
        usersproject : $resource(appConfig.api + 'users/listsupervisorproject',{code:'@code'}),
        freeactivations : $resource(appConfig.api + 'activations/listactivacionesfree',{code:'@code',user_id:'@user_id'}),
		projects : $resource(appConfig.api + 'projects/select/:projectCode', {projectCode:'@id'}),
  //activationteam : $resource(appConfig.api + 'activations/groups', {projectCode:'@id_project',number:'@number'})
        activationscampaing : $resource(appConfig.api + 'activations/listactivationscampaing',{code:'@code'})

	};
};

var SystemFactory = function(APIResources)
{
	var data = {
		projects:null,
		projectsUpdate:function()
		{
			data.projects = APIResources.projects.get();
		}
	};


	return data;
};

GeekApp.factory('APIResources', APIResources);
GeekApp.factory('SystemFactory', SystemFactory);