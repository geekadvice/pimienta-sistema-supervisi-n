GeekApp.controller('TeamController', function ($http, $scope, $routeParams, APIResources, SystemFactory)
{
	$scope.factory = SystemFactory;
	$scope.projectCode = $routeParams.projectCode;
	$scope.editProject = null;
	$scope.editedIndicator = null;

	$scope.createProject = function()
	{gulp
		$scope.editProject = {data:{name:'Nueva campaña', code:'codigo-campana', date_start:'2015-07-15', date_end:'2015-07-15'}};
	};

	$scope.saveProject = function()
	{
		$http.post(APIResources.routes.saveProject, $scope.editProject.data).success(function(){
			alert('Guardado');
			SystemFactory.projectsUpdate();
		});
	};

	$scope.deleteProject = function(_project)
	{
		$http.post(APIResources.routes.deleteProject, {id:_project.id}).success(function(){
			alert('Contenido eliminado');
			SystemFactory.projectsUpdate();
		});
	};

	$scope.editIndicator = function(_indicador)
	{
		console.log(_indicador);
		$scope.editedIndicator = _indicador;
	};

	$scope.deleteIndicator = function(_indicator)
	{
		console.log(_indicator);
		$http.post(APIResources.routes.deleteIndicator, {id:_indicator.id}).success(function(){
			alert('Indicador eliminado');
			$scope.editProject = APIResources.projects.get({projectCode:$routeParams.projectCode});
		});
	};

	$scope.saveIndicator = function(_params)
	{
		console.log(_params);
		_params.project_id = $scope.editProject.data.id;
		$http.post(APIResources.routes.saveIndicator, _params).success(function(){
			alert('Indicador guardado');
			$scope.updatEditeProject();
			_params = {};
		});
	};

	$scope.updatEditeProject = function()
	{
		$scope.editProject = APIResources.projects.get({projectCode:$routeParams.projectCode});
	};

	//Controller Init
	if($routeParams.projectCode && $routeParams.projectCode != 'create')
		$scope.updatEditeProject();

	if($routeParams.projectCode == 'create')
		$scope.createProject();

	if(!$routeParams.projectCode || SystemFactory.projects == null)
		SystemFactory.projectsUpdate();

});