GeekApp.controller('ActivationController', function ($http, $scope, $routeParams, APIResources, SystemFactory,UserService)
{
    UserService.name='equipos';
    UserService.subtab=1;
    $scope.projects = APIResources.projects.get();
    $scope.projectCode = $routeParams.projectCode;

    $scope.userId = $routeParams.userID;
    $scope.code_excel=null;
    $scope.selectedProject = null;

    //asignar activaciones
    $scope.asignaractivacion=function()
    {
        var asignar = [];
        /*angular.forEach($scope.activationsfree.data, function(value, key) {
            if(value.check==true)
            {
                asignar.push(value.id);
            }
        });*/
        $http.post(APIResources.routes.addactivationssupervisor, {supervisor_id:$scope.userId,activations:$scope.activationsfree.data})
            .success(function(data){
                alert('se asignaron correctamente');
                $scope.actualizar();
            });
    };
    //actualizar activaciones
    $scope.actualizar=function(){
        $scope.activationsfree = APIResources.freeactivations.get({code:$scope.projectCode,user_id:$scope.userId },function(){
            if($scope.activationsfree.data.length!=0)
            {
                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng($scope.activationsfree.data[0].lat, $scope.activationsfree.data[0].lon)
                };
                var map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);
                var  geocoder = new google.maps.Geocoder();
                //creo mapa
                var _path = [];     // ruta
                var _poliLinea = new google.maps.Polyline({ //polilinea
                    path: _path,
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });
                //asigno polilinea al mapa
                _poliLinea.setMap(map);
                angular.forEach($scope.activationsfree.data, function(value, key) {
                    var pinColor = "FE7569";
                    if(value.check==true)
                    {
                        pinColor = "f09138";
                    }
                    var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                        new google.maps.Size(21, 34),
                        new google.maps.Point(0,0),
                        new google.maps.Point(10, 34));
                    var marker = new google.maps.Marker({
                        position:new google.maps.LatLng(value.lat, value.lon),
                        map: map,
                        icon: pinImage,
                        title:value.sub_canal

                    });
                    //creo punto a partir de la dirección dada
                    var o_LL = new google.maps.LatLng(value.lat, value.lon)
                    //agrego el punto al array de la ruta
                    _path.push(o_LL);
                    //cargo la nueva ruta en la polilinea
                    _poliLinea.setPath(_path);
                    //centro el mapa en el último punto
                    map.setCenter(o_LL);
                    //Hago un poco de zoom
                    map.setZoom(10);
                });
            }
            else
            {
                var mapOptions = {
                    zoom: 50,
                    center: new google.maps.LatLng(-16.410104, -71.537295)
                };
                var map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);
            }
        });

    }
    $scope.actualizar();




});


GeekApp.controller('ActivationGroupController', function ($http, $scope, $routeParams, APIResources, SystemFactory,UserService)
{
    $scope.projects = APIResources.projects.get();
    $scope.projectCode = $routeParams.projectCode;
    $scope.groupId = $routeParams.groupID;
    UserService.name='equipos';
    //$scope.activation=APIResources.activationteam.get();
    UserService.subtab=2;
    $scope.getActivation=function()
    {
        $http.post(APIResources.routes.listactivationgroup,{project_code:$scope.projectCode,number:$scope.groupId})
            .success(function(data){
                $scope.activations=data.data;
                if($scope.activations.data.length!=0)
                {
                    var mapOptions = {
                        zoom: 10,
                        center: new google.maps.LatLng($scope.activations.data[0].lat, $scope.activations.data[0].lon)
                    };
                    var map = new google.maps.Map(document.getElementById('map-canvas'),
                        mapOptions);
                    var  geocoder = new google.maps.Geocoder();
                    //creo mapa
                    var _path = [];     // ruta
                    var _poliLinea = new google.maps.Polyline({ //polilinea
                        path: _path,
                        strokeColor: '#FF0000',
                        strokeOpacity: 1.0,
                        strokeWeight: 2
                    });
                    //asigno polilinea al mapa
                    _poliLinea.setMap(map);
                    angular.forEach($scope.activations.data, function(value, key) {
                        var pinColor = "FE7569";
                        angular.forEach($scope.activations.activations_id, function(check, key) {
                            if(check==value.id)
                            {
                                pinColor = "f09138";
                            }
                        });

                        var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                            new google.maps.Size(21, 34),
                            new google.maps.Point(0,0),
                            new google.maps.Point(10, 34));
                        var marker = new google.maps.Marker({
                            position:new google.maps.LatLng(value.lat, value.lon),
                            map: map,
                            icon: pinImage,
                            title:value.sub_canal

                        });
                        //creo punto a partir de la dirección dada
                        var o_LL = new google.maps.LatLng(value.lat, value.lon)
                        //agrego el punto al array de la ruta
                        _path.push(o_LL);
                        //cargo la nueva ruta en la polilinea
                        _poliLinea.setPath(_path);
                        //centro el mapa en el último punto
                        map.setCenter(o_LL);
                        //Hago un poco de zoom
                        map.setZoom(10);
                    });
                }
                else
                {
                    var mapOptions = {
                        zoom: 50,
                        center: new google.maps.LatLng(-16.410104, -71.537295)
                    };
                    var map = new google.maps.Map(document.getElementById('map-canvas'),
                        mapOptions);
                }
            });
    };
    $scope.getActivation();
    //actualizar activaciones
    $scope.actualizar=function(){
        $scope.activationsfree = APIResources.freeactivations.get({code:$scope.projectCode,user_id:$scope.userId },function(){

    });

    };

    $scope.groups = {
        id: [2]
    };

    $scope.saveAssignation=function()
    {
        $http.post(APIResources.routes.groupAssignation, {team_number:$scope.groupId,project_code:$scope.projectCode,activations_id:$scope.activations.activations_id})
            .success(function(data){
                alert('Las activaciones se asignaron correctamente');
                //$scope.actualizar();
            });
    }
});
