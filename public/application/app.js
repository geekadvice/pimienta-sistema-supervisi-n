'user strict';

var GeekApp = angular.module('GeekApp', ['ngRoute', 'ngResource', 'foundation', 'ngFileUpload','mm.foundation','angucomplete-alt','checklist-model','ngMask']);

GeekApp.config(function($routeProvider){
	$routeProvider
		.when('/', { templateUrl:appConfig.views + '/dashboard'})
		.when('/projects/:projectCode?', { templateUrl:appConfig.views + '/projects', controller:'ProjectsController'})
		.when('/client-projects/:projectCode?', { templateUrl:appConfig.views + '/clientprojects', controller:'ProjectsController'})
		//.when('/team/:projectCode??', { templateUrl:appConfig.views + '/team'})
		.when('/reports/:projectCode?', { templateUrl:appConfig.views + '/reports'})
		.when('/security/:userID?',{ templateUrl:appConfig.views + '/security'})
        .when('/editprofile',{ templateUrl:appConfig.views + '/editprofile'})
		.when('/:projectCode?/supervisor/activation/:userID?',{ templateUrl:appConfig.views + '/activations'})
		.when('/:projectCode?/group/activation/:groupID?',{ templateUrl:appConfig.views + '/activations-group'})
		.when('/config',{ templateUrl:appConfig.views + '/config'})
		.otherwise({ redirectTo: '/' });
});
GeekApp.factory('UserService', function() {
    return {
        name : 'detalles',
        subtab : 1
    };
});

window.onresize = function() { checkScreenSize(); };

function checkScreenSize()
{
	if(window.innerWidth < 500)
	{
		console.log(window.innerWidth);
		document.getElementsByTagName('body')[0].className = "mobile";
	}else{
		document.getElementsByTagName('body')[0].className = "";
	}
}

