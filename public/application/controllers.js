GeekApp.controller('MenuController', function ($scope, $location,APIResources,$http) {
    $scope.user={};
    $scope.getUser=function()
    {
        $http.post(APIResources.routes.currentUser)
        .success(function(data){
            $scope.user=data.data;

            if($scope.user.roles[0].display_name=='Administrador')
            {
                 $scope.sections = [
                    {label: 'Inicio', icon: 'icon-grid', href:'#/'},
                    {label: 'Campañas', icon: 'icon-chemistry', href:'#projects'},
                    {label: 'Supervisión', icon: 'icon-eyeglasses', href:'#reports'},                    
                    {label: 'Seguridad', icon: 'icon-lock', href:'#security'},
                    //{label: 'Ajustes', icon: 'icon-settings', href:'#config'}
                ];
            }

            if($scope.user.roles[0].display_name=='Productor')
            {
                 $scope.sections = [
                    {label: 'Inicio', icon: 'icon-grid', href:'#/'},
                    {label: 'Campañas', icon: 'icon-chemistry', href:'#projects'},
                    {label: 'Supervisión', icon: 'icon-eyeglasses', href:'#reports'}                  
                ];
            }

            if($scope.user.roles[0].display_name=='Supervisor')
            {
                 $scope.sections = [
                    {label: 'Inicio', icon: 'icon-grid', href:'#/'},            
                    {label: 'Supervisión', icon: 'icon-eyeglasses', href:'#reports'}      
                ];
            }

            if($scope.user.roles[0].display_name=='Cliente')
            {
                 $scope.sections = [
                    {label: 'Inicio', icon: 'icon-grid', href:'#/'},
                    {label: 'Campañas', icon: 'icon-chemistry', href:'#client-projects'}          
                ];
            }
        });
    };
    $scope.getUser();

	$scope.isSelected = function(_section)
	{
		var clase = "";
		var path = $location.path();
		if(path.length > 1) path = path.substring(1);
		if(path.indexOf(_section.href.substring(1)) == 0)
			clase = 'selected';
		return clase;
	};        
});

GeekApp.controller('HomeController', function ($scope, SystemFactory) {
	$scope.factory = SystemFactory;
	$scope.users = [
		{'name': 'Admin', 'email': 'gerson@geekadvice.pe'},
		{'name': 'Gerson', 'email': 'gerson@geekadvice.pe'},
		{'name': 'Roberto Gonzales', 'email': 'gerson@geekadvice.pe'},
		{'name': 'Pedro Martinez', 'email': 'gerson@geekadvice.pe'},
		{'name': 'Otro nombre', 'email': 'gerson@geekadvice.pe'}
	];

});

GeekApp.controller('ProjectsController', function ($http, $scope, $routeParams, APIResources, SystemFactory,Upload,$timeout,UserService)
{

    $scope.factory = SystemFactory;
    $scope.projectCode = $routeParams.projectCode;
    $scope.editProject = null;
    $scope.editedIndicator = {};
    $scope.editedActivation = {};
    $scope.formCollaborator={};
    $scope.formActivation={};
    $scope.preview=null;
    $scope.excel = {progress: '0% ',value:0};
    $scope.bar=false;
    $scope.usersproject = APIResources.usersproject.get({code:$routeParams.projectCode});
    $scope.formSelectedUser = null;//autocompletar supervisor
    $scope.formSelectedClient = null;//autocompletar cliente
    $scope.formSelectedProducer = null;//autocompletar cliente
    $scope.formGroup = null;//autocompletargrupo
    $scope.collaborator=null;
    $scope.editCollaborator='false';
    $scope.tabs = [];
    $scope.user={};
    $scope.getUser=function()
    {
        $http.post(APIResources.routes.currentUser)
            .success(
            function(data){
                $scope.user=data.data;

            });
    };
    $scope.getUser();
    $scope.selectedtab=function()
    {
        if(UserService.name=='detalles')
        {
            $scope.tabs = [
                {active: true},
                {active: false},
                {active: false},
                {active: false},
                {active: false}
            ];
            $scope.tab=1;
        }
        else
        {
            $scope.tabs = [
                {active: false},
                {active: false},
                {active: false},
                {active: true},
                {active: false}
            ];
            $scope.tab=UserService.subtab;
        }
    }
    $scope.selectedtab();
    $scope.selectstatus=function(id,status,index){
        $scope.statusActivation=status;
        $scope.activID=id;
        $scope.idx=index;
    };
    $scope.UpdatestatusActivation=function()
    {
        $http.post(APIResources.routes.statusactivation,{status:$scope.statusActivation,activation_id:$scope.activID})
            .success(function(data){
               alert('estado de activacion cambiado')
                $scope.editProject.data.activations[$scope.idx].status=$scope.statusActivation;
            });
    };
    $scope.select=function(_num){
        if(_num!=4){
            UserService.name='detalles';
        }
        for(var i=0;i<5;i++)
        {
            $scope.tabs[i].active=false;
            if(i==_num)
            {
                $scope.tabs[i].active=true;
            }
        }
    };

    $scope.editCollaborator = function(_id,_number,_rol)
    {

        $http.post(APIResources.routes.editCollaborator,JSON.stringify({id:_id,number:_number,rol:_rol})).success(function(){
            alert('Guardado');
            $scope.listCollaborator();
            $scope.groupCollaborator();
        });
    };

    $scope.groupCollaborator = function()
    {
        $http.post(APIResources.routes.groupCollaborator,{project_code:$scope.projectCode})
            .success(function(data){
                $scope.group=data.data;
            });
    };
    $scope.groupCollaborator();

    $scope.listCollaborator = function()
    {
        $http.post(APIResources.routes.listCollaborator,{project_code:$scope.projectCode})
        .success(function(data){
                $scope.collaborator=data.data;
        });
    };

    $scope.saveCollaborator = function()
    {
        $scope.formCollaborator.project_code=$scope.projectCode;
        $http.post(APIResources.routes.saveCollaborator,JSON.stringify($scope.formCollaborator)).success(function(){
            alert('Guardado');
            $scope.formCollaborator={};
            $scope.$broadcast('angucomplete-alt:clearInput','collaboratorid');
            $scope.listCollaborator();
            $scope.groupCollaborator();
        });
    };

    $scope.deleteCollaborator=function(_id)
    {
        $http.post(APIResources.routes.deleteCollaborator, {id:_id}).success(function(){
            alert('El colaborador se elimino con éxito');
            $scope.listCollaborator();
            $scope.groupCollaborator();
        });
    };

    $scope.deleteSupervisor=function(_id)
    {
        $http.post(APIResources.routes.deleteSupervisor, {id:_id,code:$scope.projectCode}).success(function(){
            alert('El Supervisor se elimino con éxito');
            $scope.usersproject = APIResources.usersproject.get({code:$routeParams.projectCode});
        });
    };

    $scope.setFormUser = function(_supervisor)
    {
        $scope.formSelectedUser = _supervisor.originalObject;
    };
    $scope.setFormclient = function(_client)
    {
        $scope.formSelectedClient = _client.originalObject;
    };
    $scope.setFormproducer = function(_producer)
    {
        $scope.formSelectedProducer = _producer.originalObject;
    };

    $scope.setFormCollaborator = function(_collaborator)
    {
        $scope.formCollaborator = _collaborator.originalObject;
    };
    //asignar supervisor a la campaña
    $scope.saveUsercampania=function()
    {
        if($scope.formSelectedUser!=null)
        {
            $http.post(APIResources.routes.saveUsercamp, {user_id:$scope.formSelectedUser.id,code_project:$scope.projectCode}).success(function(data){
                alert(data.status.description);
                $scope.usersproject = APIResources.usersproject.get({code:$routeParams.projectCode});
                $scope.formSelectedUser=null;
                $scope.$broadcast('angucomplete-alt:clearInput','members');

            });
        }
        else
        {
            alert('no se selecciono ningun supervisor');
        }
    };
    $scope.previewImage=function(_path)
    {
        $scope.preview=_path;
    };

    $scope.saveActivation=function()
    {
        $http.post(APIResources.routes.saveActivation, {activation_id:$scope.editedActivation.id,observation:$scope.editedActivation.observation}).success(function(){
            alert('Guardado');
        });
    };

    $scope.deleteImage=function(_id)
    {
        $http.post(APIResources.routes.deleteImage, {id:_id}).success(function(){
            alert('La image se elimino con éxito');
            $scope.listImage($scope.editedActivation.id);
        });
    };

    $scope.listImage=function(_id)
    {
        $http.post(APIResources.routes.images, {activation_id:_id})
            .success(function(data){
            $scope.editedActivation.images=data.data;
        });
    };

    $scope.selectActivation=function(_id)
    {
        $http.post(APIResources.routes.findActivation, {activation_id:_id})
            .success(function(data){
                $scope.editedActivation=data.data;
            });
    };
    $scope.deleteActivation=function(_id)
    {

            if (confirm("Al eliminar esta activación se perdera toda esta información. ¿Esta seguro que desea continuar?") == true) {
                $http.post(appConfig.api + 'activations/deleteactivation',{activation_id : _id})
                    .success(function (data) {
                        alert('Activación Eliminada');
                        $scope.updatEditeProject();
                    })
            }

    }
    $scope.$watch('files', function () {
        $scope.upload($scope.files);
    });
    $scope.log = '';

    $scope.upload = function (files) {

        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {

                var file = files[i];
                Upload.upload({
                    url: APIResources.routes.saveActivationImage,
                    fields: {
                        'activation_id': $scope.editedActivation.id
                    },
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.log = 'progress: ' + progressPercentage + '% ' +
                    evt.config.file.name + '\n' + $scope.log;
                }).success(function (data, status, headers, config) {
                    $timeout(function() {
                        $scope.log = 'file: ' + config.file.name + ', Response: ' + JSON.stringify(data) + '\n' + $scope.log;
                    });
                    alert('La imagen se subio con éxito')
                    $scope.selectActivation($scope.editedActivation.id);

                });
            }
        }
        SystemFactory.projectsUpdate();
    };

    $scope.uploadExcel = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                $scope.bar=true;
                var file = files[i];
                Upload.upload({
                    url: APIResources.routes.loadExcel,
                    fields: {
                        'code_project': $scope.projectCode
                    },
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.excel = {progress:progressPercentage + '% ',value:progressPercentage};
                    //evt.config.file.name + '\n' + $scope.log;
                }).success(function (data, status, headers, config) {
                    $timeout(function() {
                        $scope.log = 'file: ' + config.file.name + ', Response: ' + JSON.stringify(data) + '\n' + $scope.log;
                    });
                    alert('La archivo excel se subio con éxito');
                    $scope.bar=false;
                    $scope.updatEditeProject();
                    $scope.excel = {progress: '0% ',value:0};
                });
            }
        }
    };

	$scope.createProject = function()
	{
		$scope.editProject = {data:{name:'', code:'', date_start:'', date_end:'',status:1}};
	};

	$scope.saveProject = function()
	{
        if($scope.formSelectedClient!=null)
        {
            $scope.editProject.data.client_id=$scope.formSelectedClient.id;
        }
        if($scope.formSelectedProducer!=null)
        {
            $scope.editProject.data.producer_id=$scope.formSelectedProducer.id;

        }
		$http.post(APIResources.routes.saveProject, $scope.editProject.data).success(function(data){
            if(data.status.code==200)
            {
                alert('Guardado');
              //  $scope.resetformProject();
                SystemFactory.projectsUpdate();
            }
            else
            {

                var error='';
                angular.forEach(data.status.details, function(value, key) {
                    error=error.concat(value);
                    error=error.concat('\n');
                });
                alert(data.status.description +'\n'+ error);

            }


		});
	};

	$scope.deleteProject = function(_project)
	{
		$http.post(APIResources.routes.deleteProject, {id:_project.id}).success(function(){
			alert('Contenido eliminado');
			SystemFactory.projectsUpdate();
		});
	};

	$scope.editIndicator = function(_indicador)
	{
		console.log(_indicador);
		$scope.editedIndicator = _indicador;
	};

	$scope.deleteIndicator = function(_indicator)
	{
		console.log(_indicator);
		$http.post(APIResources.routes.deleteIndicator, {id:_indicator.id}).success(function(){
			alert('Indicador '+_indicator.name+' eliminado');
			$scope.editProject = APIResources.projects.get({projectCode:$routeParams.projectCode});
		});
	};

	$scope.saveIndicator = function(_params,formIndicator)
	{
		console.log(_params);

		_params.project_id = $scope.editProject.data.id;
        if(formIndicator.$valid)
        {
            $http.post(APIResources.routes.saveIndicator, _params).success(function(){
                $scope.editedIndicator={};
                alert('Indicador guardado');
                $scope.resetformIndicator(formIndicator);
                $scope.updatEditeProject();
                _params = {};
            });
        }
        else
        {
            alert('completar campos requeridos');
        }

	};
    $scope.resetformIndicator = function(formIndicator) {

        formIndicator.$setPristine();
        formIndicator.$setUntouched();


    };

    $scope.resetformProject = function(formProject) {

        formProject.$setPristine();
        formProject.$setUntouched();


    };

	$scope.updatEditeProject = function()
	{
		$scope.editProject = APIResources.projects.get({projectCode:$routeParams.projectCode});
	};

	//Controller Init
	if($routeParams.projectCode && $routeParams.projectCode != 'create')
		$scope.updatEditeProject();

	if($routeParams.projectCode == 'create')
		$scope.createProject();

	if(!$routeParams.projectCode || SystemFactory.projects == null)
		SystemFactory.projectsUpdate();

});

GeekApp.controller('SecurityController', function ($http, $scope, $routeParams, APIResources,Upload) {

	$scope.formData = {};
    $scope.reset = function(formUser) {
        if (formUser) {
            formUser.$setPristine();
            formUser.$setUntouched();
        }

    };
    $scope.reset();
    $scope.statusUser=function(_status)
    {
         $http.post(APIResources.routes.statusUser,
            {
                user_id:$scope.users[$scope.userID].id,
                status:_status
            })
            .success(function(data){
                if(_status=='0')
                    alert('Usuario Habilitado');
                else
                    alert('Usuario Deshabilitado');               
                $scope.initForm();
            });
    }

	$scope.initForm = function()
	{
		if( typeof $scope.userID === 'undefined' || $scope.userID === '-1' )
		{
			$scope.formData = {};
            $scope.formData.roles = 0;
            $scope.formData.company='';
            $scope.formData.path = 'img/avatar/001.png';
		}
		else
		{
            $scope.formData.id=$scope.users[$scope.userID].id;
			$scope.formData.name = $scope.users[$scope.userID].name;
            $scope.formData.dni = $scope.users[$scope.userID].dni;
            $scope.formData.disabled = $scope.users[$scope.userID].disabled;
            $scope.formData.last_name = $scope.users[$scope.userID].last_name;
			$scope.formData.email = $scope.users[$scope.userID].email;
			$scope.formData.rol = $scope.users[$scope.userID].roles[0].id;
            $scope.formData.company = $scope.users[$scope.userID].company;
            $scope.formData.roles = 1;
			$scope.formData.display_name = $scope.users[$scope.userID].roles[0].display_name;
			$scope.formData.description = $scope.users[$scope.userID].roles[0].description;
			if($scope.users[$scope.userID].path) $scope.formData.path = $scope.users[$scope.userID].path;
		}
	}

	$scope.initUser = function()
	{
		$http.get(appConfig.api + 'users/getall').success(function(_data){
            $scope.filterRoles={};

				$scope.users = _data.data.users;
                $scope.filterRoles = _data.data.roles;
                $scope.filterRoles.unshift({id:0,display_name:'Todos',name:'Todos'});
			});
        $http.get(appConfig.api + 'users/getall').success(function(_data){
            $scope.roles={};
            $scope.roles = _data.data.roles;
            $scope.initForm();
        });
	};


	$scope.initUser();
	
	$scope.saveUser = function(_id,formUser)
	{

            if($scope.userID !== '-1') //actualiza
            {
                $scope.formData.personid = $scope.users[_id].id;

                $http.post(appConfig.api + 'users/save',{personid : $scope.users[_id].id, name : $scope.formData.name,dni : $scope.formData.dni,	last_name : $scope.formData.last_name,		email : $scope.formData.email, rol : $scope.formData.rol , password: $scope.formData.password, company:$scope.formData.company})
                    .success(function (data) {
                        alert('Se guardado correctamente');
                        console.log(data);
                        $scope.initUser();
                        $scope.reset(formUser);
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                    });
            }
            else
            {
                $http.post(appConfig.api + 'users/save',{ name : $scope.formData.name,dni : $scope.formData.dni,	last_name : $scope.formData.last_name,		email : $scope.formData.email, rol : $scope.formData.rol , password: $scope.formData.password, company:$scope.formData.company})
                    .success(function (data) {
                        alert('Usuario creado correctamente');
                        console.log(data);
                        $scope.initUser();
                        $scope.reset(formUser);
                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                    });
            }



	};

	$scope.userID = $routeParams.userID;

	$scope.changeRol = function()
	{
		$scope.idrol = $scope.formData.rol - 1;
		$scope.formData.display_name = $scope.roles[$scope.idrol].display_name;
		$scope.formData.description = $scope.roles[$scope.idrol].description;
	}


    $scope.changeRoles = function(_id)
    {
            $http.post(APIResources.routes.rolUser,{role:$scope.formData.roles})
            .success(function(data){
                //alert('hola');
                $scope.users=data.data;
               //$scope.users = _data.data;
                //$scope.initForm();
            });
    }
    $scope.upload = function (files) {

        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: APIResources.routes.saveUserImage,
                    fields: {
                        userid:$scope.formData.id
                    },
                    file: file
                }).success(function (data, status, headers, config) {
                    alert('La imagen se subio con éxito');
                    $scope.initUser();
                });
            }
        }
    };

});

GeekApp.controller('editprofileController', function ($http, $scope, $routeParams, APIResources, SystemFactory,Upload,$timeout,UserService)
{
    $scope.formData={};

    $scope.UpdateUser=function(){
       $scope.formData.name= document.getElementById("name").value;
        $scope.formData.last_name=document.getElementById("last_name").value;
        $scope.formData.dni= document.getElementById("dni").value;
        $scope.formData.email=document.getElementById("email").value;
        if(document.getElementById("password").value)
        {
            $scope.formData.password= document.getElementById("password").value;
        }
        console.log($scope.formData);
        $http.post(APIResources.routes.editProfile,{name: $scope.formData.name,last_name: $scope.formData.last_name,dni:$scope.formData.dni,email: $scope.formData.email,password: $scope.formData.password})
            .success(function(data){
                alert('Se guardado correctamente');
            });
    };

    $scope.upload = function (files) {

        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: APIResources.routes.saveUserImage,
                    fields: {
                    },
                    file: file
                }).success(function (data, status, headers, config) {
                    alert('La imagen se subio con éxito');
                        document.getElementById("path").src=data.data.path;

                });
            }
        }
    };
});
