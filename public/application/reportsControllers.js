GeekApp.controller('ReportsController', function ($http, $scope, $routeParams, APIResources, SystemFactory,Upload,$timeout)
{
	$scope.projects = APIResources.projects.get();
	$scope.projectCode = $routeParams.projectCode;
    $scope.code_excel=null;
    $scope.selectedProject = null;
    $scope.activations=null;
    $scope.indicators=null;
    $scope.activation_id=0;
    $scope.enable_activation=true;
    $scope.options=[{value:'Si'},
        {value:'No'}];
    $scope.ind=[];
    $scope.ind2=[];
     $scope.$watch('file', function (file) {
      $scope.upload($scope.file);
    });

    $scope.previewImage=function(_path)
    {
        $scope.preview=_path;
    };

    $scope.deleteImage=function(_id)
    {
        $http.post(APIResources.routes.deleteImage, {id:_id}).success(function(){
            alert('La image se elimino con éxito');
            $scope.selectactivation($scope.activation_id);
        });
    };

    $scope.upload = function (files,_indicatorid) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                $scope.bar=true;
                var file = files[i];
                Upload.upload({
                    url: APIResources.routes.saveIndicatorImage,
                    fields: {'activation_id': $scope.activation_id,'indicator_id':_indicatorid},
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.imgPercent = {progress:progressPercentage + '% ',value:progressPercentage};
                    //evt.config.file.name + '\n' + $scope.log;
                }).success(function (data, status, headers, config) {
                    $timeout(function() {
                        $scope.log = 'file: ' + config.file.name + ', Response: ' + JSON.stringify(data) + '\n' + $scope.log;
                    })
                    $scope.selectactivation($scope.activation_id);
                    alert('La imagen se subio con éxito');
                    $scope.bar=false;                    
                    $scope.imgPercent = {progress: '0% ',value:0};
                });
            }
        }
        //$scope.selectactivation($scope.activation_id);
        
    };

    $scope.selectactivation=function(id,status)
    {
        $scope.activation_id=id;
        $http.post(appConfig.api + 'activations/activationindicators',{activation_id : $scope.activation_id})
            .success(function (data) {
                if(data.data.length!=0) {
                    if (data.data[0].activation.status == '2' || data.data[0].activation.status == '1') {
                        $scope.enable_activation = false;
                    }
                    else{
                        $scope.enable_activation = true;
                        }
                    $scope.ind=$scope.ind2;
                    angular.forEach($scope.ind, function(indic, key) {
                        angular.forEach(data.data, function(value, key) {
                            if(value.indicator.id==indic.id)
                            {
                                if(value.indicator.type=='imagen')
                                {
                                    indic.path=value.images;
                                }
                                else if(value.indicator.type=='cualitativo')
                                {
                                    indic.value=parseInt(value.value)
                                }
                                else if (value.indicator.type=='cuantitativo')
                                {
                                    indic.value=parseInt(value.value)
                                }
                                else
                                {
                                    indic.value=value.value;
                                }
                            }
                        });
                    });
                }
                else
                {
                    if(status=='2' || status=='1'){
                        $scope.enable_activation=false;
                    }
                    else
                        $scope.enable_activation=true;
                    var inds = [];
                    angular.forEach($scope.indicators, function(value, key) {

                        if(value.type=='imagen')
                        {
                            inds.push({name: value.name,type:value.type,value:'',id:value.id,group:value.group,path:[]});
                        }
                        else if(value.type=='cualitativo')
                        {
                            inds.push({name: value.name,type:value.type,value:0,id:value.id,group:value.group});
                        }
                        else
                            inds.push({name: value.name,type:value.type,value:'',id:value.id,group:value.group});

                    });
                    $scope.ind=inds;
                    $scope.ind2=inds;
                }
            })
    };
    $scope.generateReport=function()
    {

        if($scope.activation_id!=0)
        {
            if (confirm("Cuando genere el reporte, la activación se cerrara y no se podran editar sus datos. ¿Desea continuar?") == true) {
                $http.post(appConfig.api + 'activations/savereport',{activation_id : $scope.activation_id,indicators:$scope.ind})
                    .success(function (data) {
                        alert('reporte guardado');
                        $scope.selectProject($scope.selectedProject);
                        $scope.activation_id=0;
                    })
            }
        }
        else
        {
            alert('seleccione una activación');
        }
    }
    $scope.selectProject= function (_project) {
        $scope.code=  _project.code;
        $scope.selectedProject = _project;
        $http.post(appConfig.api + 'activations/listactivationscampaing',{code : $scope.code})
            .success(function (data) {
                $scope.activations=data.data.activations;
                $scope.indicators=data.data.indicators;
                var inds = [];
                angular.forEach($scope.indicators, function(value, key) {

                    if(value.type=='imagen')
                    {
                        inds.push({name: value.name,type:value.type,value:'',id:value.id,group:value.group,path:[]});
                    }
                    else if(value.type=='cualitativo')
                    {
                        inds.push({name: value.name,type:value.type,value:0,id:value.id,group:value.group});
                    }
                    else
                        inds.push({name: value.name,type:value.type,value:'',id:value.id,group:value.group});

                 });
                $scope.ind=inds;
                $scope.ind2=inds;
                $scope.activation_id=0
            })
    };

    $scope.saveIndicator=function() {       
        if($scope.activation_id!=0)
        {
            $http.post(appConfig.api + 'activations/savereportpreview',{activation_id : $scope.activation_id,indicators:$scope.ind})
                .success(function (data) {
                    alert('reporte guardado');
                    $scope.activation_id=0;
                })
        }
    };






});