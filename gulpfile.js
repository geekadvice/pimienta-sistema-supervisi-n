var elixir = require('laravel-elixir');
var gulp = require('gulp');
var gulpBowerFiles = require('main-bower-files');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.extend('bower', function(mix) {
	return gulp.src(gulpBowerFiles()).pipe(gulp.dest('public/libs'));
});


//----------------------------------------------------------------------- Application
elixir.extend('application', function(mix) {
	mix.styles([
		'libs/foundation.css',
		'libs/foundation-apps.css',
		'css/app.css',
        'components/angucomplete-alt/angucomplete-alt.css',
	], 'public/output/all_app.css', 'public/')
		.scripts([
			'libs/angular.js',
			'libs/foundation-apps.js',
			'components/foundation-apps/dist/js/foundation-apps-templates.js',
			'libs/angular-animate.js',
			'libs/angular-resource.js',
			'libs/angular-route.js',
			'libs/angular-ui-router.js',
			'libs/ng-file-upload.js',
			'libs/fastclick.js',
			'libs/TweenMax.js',
			'application/app.js',
			'application/services.js',
			'application/controllers.js',
			'application/reportsControllers.js',
   'application/activationControllers.js',
			'application/teamControllers.js',
			'components/jquery/dist/jquery.js',
   'components/foundation/js/foundation.min.js',
   'components/angucomplete-alt/angucomplete-alt.js',
   'components/checklist-model/checklist-model.js',
   'components/angular-foundation/mm-foundation.min.js',
   'components/angular-foundation/mm-foundation-tpls.min.js',
   'components/angular-mask/dist/ngMask.min.js'
		], 'public/output/all_app.js', 'public/');
});


//----------------------------------------------------------------------- Website
elixir.extend('website', function(mix) {
	mix.styles([
		'libs/foundation.css',
		'css/site.css'
	], 'public/output/all.css', 'public')
		.scripts([
			'libs/jquery.js',
			'libs/foundation.js',
			'application/site.js'
		], 'public/output/all.js', 'public');
});

elixir(function(mix) {
	mix.less(['app.less']);
	mix.bower(mix);
	mix.application(mix);
	//mix.website(mix);
});